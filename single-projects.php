<?php get_header(); ?>

<?php while (have_posts()) : the_post();
    global $post;
    $have_metadata = false;
    $metadata = array(
        'location' => 'Location',
        'type' => 'Type',
        'size' => 'Size',
        'comments' => 'Comments',
    );
    foreach ($metadata as $meta_key => $meta_label) {
        if (get_field("meta_data_{$meta_key}")) {
            $have_metadata = true;
        }
    }

    $header_image = '';
    $terms = wp_get_object_terms(get_the_ID(), 'categories-project');
    if ($image = get_field('background_single_project', 'option')) {
        $header_image = $image['url'];
    }

    if (has_post_thumbnail()) {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'page-heading-image');
        $header_image = $image[0];
    }
?>
<header class="page-heading page-heading--project">
    <div class="wrapper">
        <p><?php echo jcd_project_back_button(get_the_ID()); ?></p>

        <h1 class="entry-title project-title mt-0 mb-0">
            <?php the_title(); ?>
        </h1>

        <?php if ($have_metadata) : ?>
            <div class="project-metadata clearfix">
                <?php foreach ($metadata as $meta_key => $meta_label) :
                    $value = get_field("meta_data_{$meta_key}");
                    if ($value) : ?>
                        <div class="project-metadata__item">
                            <div class="project-metadata__item__label"><?php echo $meta_label; ?></div>
                            <div class="project-metadata__item__value"><?php echo $value; ?></div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <?php if ($header_image != '') : ?>
        <div class="page-heading__background-image layer-cover-bg" style="background-image: url('<?php echo $header_image; ?>')"></div>
    <?php endif; ?>
</header>


<div class="main-section block-section pt-0">
    <div class="wrapper">
        <article <?php post_class(); ?>>

            <div class="project-gallery-grid grid">
                <?php if (has_post_thumbnail()) : ?>
                    <div class="project-gallery-grid__item grid__item">
                        <a class="js-image-popup project-gallery-grid__item__link" href="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(), 'full'); ?>">
                            <?php the_post_thumbnail('hero-image'); ?>
                        </a>
                    </div>
                <?php endif; ?>

                <?php if ($video_list = get_field('video_list')) : 
                    $video_counter = 1; ?>
                    <?php foreach ($video_list as $video) : ?>
                        <?php if ($video['video_file'] && $video['video_cover']) : ?>
                            <div class="project-gallery-grid__item grid__item large--one-half medium--one-half small--one-half">
                                <a class="js-video-popup project-gallery-grid__item__link project-gallery-grid__item__link--ratio" href="#player-popup-<?php echo $video_counter; ?>">
                                    <img class="video-play" src="<?php echo get_template_directory_uri(); ?>/images/arrow-play.png" alt="Play">
                                    <?php echo wp_get_attachment_image($video['video_cover']['id'], 'project-thumb', '', array('class' => 'display-block')); ?>
                                </a>
                            </div>

                            <div class="zoom-anim-dialog video-popup mfp-hide" id="player-popup-<?php echo $video_counter; ?>">
                                <div class="zoom-anim-dialog__inner video-popup__inner">
                                    <video
                                        id="video-player"
                                        class="video-js vjs-fluid"
                                        controls
                                        preload="auto"
                                        poster="<?php echo $video['video_cover']['sizes']['large']; ?>"
                                        data-setup='{}'
                                    >
                                        <source src="<?php echo $video['video_file']['url']; ?>" type="video/mp4"></source>
                                        <p class="vjs-no-js">
                                            To view this video please enable JavaScript, and consider upgrading to a
                                            web browser that
                                            <a href="http://videojs.com/html5-video-support/" target="_blank">
                                            supports HTML5 video
                                            </a>
                                        </p>
                                    </video>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php $video_counter++;
                    endforeach; ?>
                <?php endif; ?>

                <?php if ($gallery = get_field('gallery')) : ?>
                    <?php foreach ($gallery as $image) : ?>
                        <div class="project-gallery-grid__item grid__item large--one-half medium--one-half small--one-half">
                            <a class="js-image-popup project-gallery-grid__item__link project-gallery-grid__item__link--ratio" href="<?php echo wp_get_attachment_image_url($image['id'], 'full'); ?>">
                                <?php echo wp_get_attachment_image($image['id'], 'project-thumb', '', array('class' => 'display-block')); ?>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

        </article>
    </div>
</div><!-- .main-content-section -->

<?php endwhile; ?>
<?php get_footer(); ?>
<?php
/**
 * Template Name: Contact
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <article <?php post_class(); ?>>
        
        <?php get_template_part('partials/component/page-heading'); ?>

        <section class="main-section block-section pt-0">
            <div class="wrapper">
                <div class="grid">
                    <div class="grid__item large--eight-twelfths">
                        <div class="entry-content mb-60">
                            <?php the_content(); ?>
                        </div>
                    </div>

                    <div class="grid__item large--three-twelfths push--large--one-twelfth contact-sidebar">
                        <?php get_template_part('partials/component/contact-info'); ?>

                        <div class="contact-social">
                            <h3 class="contact-sidebar__title"><?php _e( 'Follow Us', 'jcd' ); ?></h3>
                            <?php get_template_part( 'partials/component/social', 'links' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- .main-content -->

        <?php if ($contact_maps = get_field('contact_map', 'option')) : ?>
            <div class="maps-section">
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $contact_maps['lat']; ?>" data-lng="<?php echo $contact_maps['lng']; ?>">
                        <?php echo $contact_maps['address']; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </article>

<?php endwhile; ?>

<?php get_footer(); ?>
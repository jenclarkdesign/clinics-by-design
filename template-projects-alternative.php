<?php

/**
 * Template Name: Project Alternative
 * Template Post Type: projects
 */

get_header(); ?>

<?php get_template_part('partials/component/page', 'heading'); ?>

<div class="main-section block-section pt-0">
    <div class="wrapper">
        <?php while (have_posts()) : the_post();
        global $post;
        ?>
            <article <?php post_class('grid'); ?>>
                <?php if ($post->post_content) : ?>
                    <div class="grid__item large--four-twelfths">
                        <div class="single-project__content">
                            <?php jcd_project_back_button(get_the_ID()); ?>
                            <div class="entry-content"><?php the_content(); ?></div>
                        </div>
                    </div>
                <?php endif; ?>
                
                <?php if ($post->post_content) : ?>
                    <div class="grid__item large--eight-twelfths">
                <?php else : ?>
                    <div class="grid__item large--eight-twelfths push--large--two-twelfths">
                <?php endif; ?>

                    <p><?php echo jcd_project_back_button(get_the_ID()); ?></p>
                
                    <div class="project-gallery-slides js-gallery-slides">
                        <?php if (has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail('large'); ?>
                        <?php endif; ?>

                        <?php if ($gallery = get_field('gallery')) : ?>
                            <?php foreach ($gallery as $image) : ?>
                                <?php echo wp_get_attachment_image($image['id'], 'large'); ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <div class="project-gallery-thumbnails js-gallery-thumbs grid grid-uniform">
                        <?php if (has_post_thumbnail()) : ?>
                            <button class="project-gallery-thumbnails__item active grid__item three-twelfths" data-index="0">
                                <?php the_post_thumbnail('single-gallery'); ?>
                            </button>
                        <?php endif; ?>

                        <?php if ($gallery = get_field('gallery')) :
                            $counter = 1; ?>
                            <?php foreach ($gallery as $image) : ?>
                                <button class="project-gallery-thumbnails__item grid__item small--three-twelfths medium--three-twelfths large--three-twelfths" data-index="<?php echo $counter; ?>">
                                    <?php echo wp_get_attachment_image($image['id'], 'single-gallery'); ?>
                                </button>
                            <?php $counter++;
                            endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </article>
        <?php endwhile; ?>
    </div>
</div><!-- .main-content-section -->

<?php get_footer(); ?>

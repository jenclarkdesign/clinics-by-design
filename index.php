<?php get_header(); ?>

<?php get_template_part( 'partials/component/page-heading' ); ?>

<div class="main-content wrapper block-section pt-0">
    <div class="grid">
        <div class="grid__item large--eight-twelfths">
            <?php if ( have_posts() ) : ?>
                <div class="blog-list">
                    <div class="grid grid-uniform">
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php 
                            global $loop;
                            $loop['columns'] = '1/2';
                            get_template_part( 'partials/content/blog-item' ); 
                            ?>
                        <?php endwhile; ?>
                    </div>
                </div>

                <?php jcd_pagination(); ?>
            <?php else : ?>

                <?php get_template_part('partials/content/no-post'); ?>

            <?php endif; ?>
        </div>

        <div class="grid__item large--four-twelfths">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<!-- /.main-content -->

<?php get_footer(); ?>

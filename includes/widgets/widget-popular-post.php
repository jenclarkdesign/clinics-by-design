<?php
/**
 * Popular Post Widget
 */
class JCD_Popular_Post extends JCD_Widget {
	var $settings = array( 'title', 'posts_per_page' );

	function __construct() {
		$widget_ops = array(
			'classname' => 'widget-popular-post',
			'description' => 'Show Popular Post',
		);
		parent::__construct( 'jcd_widget_popular_post', __('JCD - Popular Post', 'jcd'), $widget_ops );
	}

	/**
	 * Render Widget
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		$posts_per_page = $posts_per_page ? $posts_per_page : 5;

		$post_query = new WP_Query(array(
			'post_type' => 'post',
			'posts_per_page' => $posts_per_page,
			'meta_key' => '_visitor_count',
			'orderby' => 'meta_value_num',
		));

		if( $post_query->have_posts() ) {
			echo $before_widget;

			if ( $title ) {
				echo $before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title;
			}

			echo '<ul>';

			while( $post_query->have_posts() ) {
				$post_query->the_post();
				echo '<li><a href="'. get_permalink() .'">'. get_the_title() .'</a></li>';
			}

			echo '</ul>';

			wp_reset_postdata();

			echo $after_widget;
		}
	}

	/**
	 * Render Form
	 */
	function form( $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('posts_per_page'); ?>"><?php _e('Number of Post to show:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('posts_per_page'); ?>"  value="<?php echo esc_attr( $posts_per_page ); ?>" class="widefat" id="<?php echo $this->get_field_id('posts_per_page'); ?>" />
		</p>
		<?php
	}
}

register_widget( 'JCD_Popular_Post' );

<?php
/**
 * Social Link Widget
 */
class JCD_Social_Link extends JCD_Widget {
	var $settings = array( 'title' );

	function __construct() {
		$widget_ops = array(
			'classname' => 'widget-social-link',
			'description' => 'Show Social Link',
		);
		parent::__construct( 'jcd_widget_social_link', __('JCD - Social Links', 'jcd'), $widget_ops );
	}

	/**
	 * Render Widget
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		echo $before_widget;

		if ( $title ) {
			echo $before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title;
		}

		get_template_part( 'partials/component', 'social-links' );

		echo $after_widget;
	}

	/**
	 * Render Form
	 */
	function form( $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
			<span class="description">You can edit social link from <a href="<?php echo admin_url('admin.php?page=jcd'); ?>" target="_blank">Theme Options</a>
			</span>
		</p>
		<?php
	}
}

register_widget('JCD_Social_Link');

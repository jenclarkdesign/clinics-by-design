<?php
/*---------------------------------------------------------------------------------*/
/* Twitter widget */
/*---------------------------------------------------------------------------------*/
class jcd_Twitter_Widget extends WP_Widget {
  	var $settings = array( 'title', 'limit', 'username' );

  	function __construct() {
		$widget_ops = array(
			'classname' => 'widget-twitter',
			'description' => 'Add your Twitter feed to your sidebar with this widget.'
		);
		parent::__construct( false, __( 'JCD - Twitter Stream', 'jcd' ), $widget_ops);
  	}


	/**
	* Render Widget
	*/
	function widget( $args, $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );

		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		echo $before_widget;

		if ( $title ) {
			echo $before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title;
		}

		$user_timeline = $this->get_user_timeline( $username, $limit );

		if( isset( $user_timeline['error'] ) ) {
			echo "<p>{$user_timeline['error']}</p>";
		} else {
			$this->build_twitter_markup( $user_timeline );
		}


		echo $after_widget;
	}


	/**
	* Save Widget
	*/
	function update( $new_instance, $old_instance ) {
		$new_instance = $this->jcd_enforce_defaults( $new_instance );
		return $new_instance;
	}


	/**
	* Render Form
	*/
	function form( $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('username'); ?>"><?php _e('Username:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('username'); ?>"  value="<?php echo esc_attr( $username ); ?>" class="widefat" id="<?php echo $this->get_field_id('username'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Number of tweet:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('limit'); ?>"  value="<?php echo $limit; ?>" class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" />
  		</p>
		<?php
	}


	/**
	* Linkify Twitter Text
	*
	* @param string s Tweet
	*
	* @return string a Tweet with the links, mentions and hashtags wrapped in <a> tags
	*/
	function linkify_twitter_text($tweet){
		$url_regex = '/((https?|ftp|gopher|telnet|file|notes|ms-help):((\/\/)|(\\\\))+[\w\d:#@%\/\;$()~_?\+-=\\\.&]*)/';
		$tweet = preg_replace($url_regex, '<a href="$1" target="_blank">'. "$1" .'</a>', $tweet);
		$tweet = preg_replace( array(
		  '/\@([a-zA-Z0-9_]+)/',    # Twitter Usernames
		  '/\#([a-zA-Z0-9_]+)/'    # Hash Tags
		), array(
		  '<a href="http://twitter.com/$1" target="_blank">@$1</a>',
		  '<a href="http://twitter.com/search?q=%23$1" target="_blank">#$1</a>'
		), $tweet );

		return $tweet;
	}


	/**
	 * Get User Timeline
	 *
	 */
	function get_user_timeline( $username = '', $limit = 5 ) {
	  $key = "twitter_user_timeline_{$username}_{$limit}";

	  // Check if cache exists
	  $timeline = get_transient( $key );
	  if ($timeline !== false) {
	    return $timeline;
	  } else {
	    $headers = array( 'Authorization' => 'Bearer ' . $this->get_access_token() );
	    $response = wp_remote_get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name={$username}&count={$limit}", array( 'headers' => $headers ));
	    if ( is_wp_error($response) ) {
	      // In case Twitter is down we return error
	      return array('error' => __('There is problem fetching twitter timeline', 'nesia'));
	    } else {
	      // If everything's okay, parse the body and json_decode it
	      $json = json_decode(wp_remote_retrieve_body($response));

	      // Check for error
	      if( !count( $json ) ) {
	        return array('error' => __('There is problem fetching twitter timeline', 'nesia'));
	      } elseif( isset( $json->errors ) ) {
	        return array('error' => $json->errors[0]->message);
	      } else {
	        set_transient( $key, base64_encode(serialize($json)), 60 * 60 );
	        return $json;
	      }
	    }
	  }
	}


	/**
	 * Get Twitter application-only access token
	 * @return string Access token
	 */
	function get_access_token() {
	  $consumer_key = 'oCWX9jjN9U5ger0LIUIfKA';
	  $consumer_secret = 'EFrPmVqxaAuQwXuGuLtR9qlvRN3ymGoewMRrF782CE';

	  $consumer_key = urlencode( $consumer_key );
	  $consumer_secret = urlencode( $consumer_secret );
	  $bearer_token = base64_encode( $consumer_key . ':' . $consumer_secret );

	  $oauth_url = 'https://api.twitter.com/oauth2/token';

	  $headers = array( 'Authorization' => 'Basic ' . $bearer_token );
	  $body = array( 'grant_type' => 'client_credentials' );

	  $response = wp_remote_post( $oauth_url, array(
	    'headers' => $headers,
	    'body' => $body
	  ) );

	  if( !is_wp_error( $response ) ) {
	    $response_json = json_decode( $response['body'] );

	    return $response_json->access_token;
	  } else {
	    return false;
	  }
	}


	/**
	 * Builder Twitter timeline HTML markup
	 */
	function build_twitter_markup( $timelines ) {
		if( is_string( $timelines ) ) {
			$timelines = unserialize( base64_decode($timelines) );
		}?>
		<ul>
			<?php foreach( $timelines as $item ) :
				$screen_name = $item->user->screen_name;
				$profile_link = "http://twitter.com/{$screen_name}";
				$status_url = "http://twitter.com/{$screen_name}/status/{$item->id}";

				$delta_time = time() - strtotime($item->created_at);
				if( $delta_time > 0 && $delta_time < 60 * 60 * 24 * 12 ) {
					$timediff = human_time_diff( strtotime($item->created_at), current_time('timestamp') ) . ' ago';
				} else {
					$timediff = date('M j, Y', strtotime($item->created_at));
				}
				?>

				<li>
					<div class="twitter-head mb-10">
						<strong><?php echo "@{$screen_name}"; ?></strong>
						<a class="twitter-timestamp" href="<?php echo $status_url; ?>"><?php echo $timediff; ?></a>
					</div>
					<div class="twitter-content"><?php echo $this->linkify_twitter_text( $item->text ); ?></div>
				</li>

			<?php endforeach; ?>
		</ul>
		
		<a class="font-weight-bold text-color-primary" href="<?php echo $profile_link; ?>">See More &rarr;</a>
		<?php
	}

	/**
	* Enforce Defaults
	*/
	function jcd_enforce_defaults( $instance ) {
		$defaults = $this->jcd_get_settings();
		$instance = wp_parse_args( $instance, $defaults );

		return $instance;
	}


	/**
	* Provides an array of the settings with the setting name as the
	* key and the default value as the value
	* This cannot be called get_settings() or it will override
	* WP_Widget::get_settings()
	*/
	function jcd_get_settings() {
		// Set the default to a blank string
		$settings = array_fill_keys( $this->settings, '' );
		// Now set the more specific defaults
		return $settings;
	}

}

register_widget( 'jcd_Twitter_Widget' );

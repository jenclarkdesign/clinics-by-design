<?php
/**
 * Mailchimp Subscribe Widget
 */
class JCD_Mailchimp_Subscribe extends JCD_Widget {
	var $settings = array( 'title', 'form_action_url' );

	function __construct() {
		$widget_ops = array(
			'classname' => 'widget-mailchimp-subscribe',
			'description' => 'Show Mailchimp subscribtion form',
		);
		parent::__construct( 'jcd_widget_mailchimp_subscribe', __('JCD - Mailchimp Subscribtion Form', 'jcd'), $widget_ops );
	}

	/**
	 * Render Widget
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		echo $before_widget;

		if ( $title ) {
			echo $before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title;
		}
		?>

		<?php if( $form_action_url ) : ?>
			<form action="<?php echo $form_action_url; ?>" method="post">
				<input type="text" name="EMAIL" placeholder="Email">
				<input type="submit" name="subscribe" value="Subscribe" class="button button-primary">
			</form>
		<?php endif; ?>

		<?php
		echo $after_widget;
	}

	/**
	 * Render Form
	 */
	function form( $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('form_action_url'); ?>"><?php _e('Mailchimp Form Action URL:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('form_action_url'); ?>"  value="<?php echo esc_attr( $form_action_url ); ?>" class="widefat" id="<?php echo $this->get_field_id('form_action_url'); ?>" /><span class="description">You can get this form action URL when creating a custom form on mailchimp.</span>
		</p>
		<?php
	}
}

register_widget( 'JCD_Mailchimp_Subscribe' );

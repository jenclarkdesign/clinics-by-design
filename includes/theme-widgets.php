<?php
/**
 * Load All Custom Widgets
 */
$widgets = array(
	'includes/widgets/widget-twitter.php',
	'includes/widgets/widget-facebook-like-box.php',
	//'includes/widgets/widget-donation.php',
	//'includes/widgets/widget-about-box.php',
	//'includes/widgets/widget-mailchimp-subscribe.php',
	// 'includes/widgets/widget-social-link.php',
	'includes/widgets/widget-popular-post.php',
);

// Allow child themes/plugins to add widgets to be loaded.
$widgets = apply_filters( 'jcd_widgets', $widgets );

foreach ( $widgets as $w ) {
	locate_template( $w, true );
}

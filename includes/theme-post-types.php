<?php
/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/

/**
* Projects Post Type
*
* This post type will be used for Project
*/
function jcd_register_projects_post() {
	$labels = array(
		'name'                => __( 'Projects', 'jcd' ),
		'singular_name'       => __( 'Project', 'jcd' ),
		'add_new'             => _x( 'Add New Project', 'jcd', 'jcd' ),
		'add_new_item'        => __( 'Add New Project', 'jcd' ),
		'edit_item'           => __( 'Edit Project', 'jcd' ),
		'new_item'            => __( 'New Project Address', 'jcd' ),
		'view_item'           => __( 'View Projects Address', 'jcd' ),
		'search_items'        => __( 'Search Projects', 'jcd' ),
		'not_found'           => __( 'No Projects found', 'jcd' ),
		'not_found_in_trash'  => __( 'No Projects found in Trash', 'jcd' ),
		'parent_item_colon'   => __( 'Parent Project:', 'jcd' ),
		'menu_name'           => __( 'Projects', 'jcd' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => '',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'menu_position'       => 4,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_icon'           => 'dashicons-admin-post',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'page',
		'supports'            => array(
			'title','editor','thumbnail' 
			),
		'show_in_rest'		  => true,
		'rest_base'			  => 'projects',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	);
  
	register_post_type( 'projects', $args );
}

add_action( 'init', 'jcd_register_projects_post' );

/**
 * Create taxonomy Category Project
 */
function jcd_taxonomy_category_project() {

	$labels = array(
		'name'					=> _x( 'Categories', 'Taxonomy Categories', 'jcd' ),
		'singular_name'			=> _x( 'Category', 'Taxonomy Category', 'jcd' ),
		'search_items'			=> __( 'Search Categories', 'jcd' ),
		'popular_items'			=> __( 'Popular Categories', 'jcd' ),
		'all_items'				=> __( 'All Categories', 'jcd' ),
		'parent_item'			=> __( 'Parent Category', 'jcd' ),
		'parent_item_colon'		=> __( 'Parent Category', 'jcd' ),
		'edit_item'				=> __( 'Edit Category', 'jcd' ),
		'update_item'			=> __( 'Update Category', 'jcd' ),
		'add_new_item'			=> __( 'Add New Category', 'jcd' ),
		'new_item_name'			=> __( 'New Category Name', 'jcd' ),
		'add_or_remove_items'	=> __( 'Add or remove Categories', 'jcd' ),
		'choose_from_most_used'	=> __( 'Choose from most used jcd', 'jcd' ),
		'menu_name'				=> __( 'Category', 'jcd' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => false,
		'hierarchical'      => true,
		'show_tagcloud'     => true,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'project-categories' ),
		'query_var'         => true,
		'capabilities'      => array(),
		// 'show_in_rest'		  => true,
		// 'rest_base'			  => 'categories-project',
		// 'rest_controller_class' => 'WP_REST_Terms_Controller',
	);

	register_taxonomy( 'categories-project', array( 'projects' ), $args );
}
add_action( 'init', 'jcd_taxonomy_category_project' );
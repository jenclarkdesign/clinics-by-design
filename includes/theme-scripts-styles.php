<?php
/**
 * Hook into enqueue_scripts to enqueue scripts and styles
 *
 * @package WordPress
 * @since 1.0
 */
if( !function_exists( 'jcd_scripts_styles' ) ) {
    function jcd_scripts_styles() {
        $is_debug = WP_DEBUG;

        if (isset($_GET['debug'])) {
            $is_debug = true;
        }

        $debug = $is_debug ? '' : '.min';

        // Dequeue media element style
        wp_deregister_style('wp-mediaelement');

        // Google Maps
        $api_key = array(
            'key' => acf_get_setting('google_api_key'),
            'libraries' => 'places',
            'ver' => 3,
            'callback' => ''
        );

        $maps_url = add_query_arg($api_key, 'https://maps.googleapis.com/maps/api/js');
        if (is_page_template('template-contact.php')) {
            wp_enqueue_script('google-maps', $maps_url, array(), '3', true);
        }

        // Include main style
        wp_enqueue_style('google-font', 'https://fonts.googleapis.com/css?family=Montserrat:400,700');
        wp_enqueue_style('jcd-app', get_stylesheet_directory_uri() . '/app' . $debug . '.css');
        wp_enqueue_style('jcd-main', get_stylesheet_uri());

        // Modernizr
        wp_enqueue_script('modernizr', get_template_directory_uri() . '/includes/js/modernizr.js', array(), '3.5.0', false);

        // Include the javascript
        wp_enqueue_script('jquery');
        wp_enqueue_script('jcd-scripts', get_template_directory_uri() . '/includes/js/scripts' . $debug . '.js', array('jquery'), '1.0', true);

        if (is_singular('projects')) {
            // Video.js
            wp_enqueue_style('video-js', 'https://vjs.zencdn.net/7.0.2/video-js.css');
            // wp_enqueue_script( 'video-js-ie8', 'http://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js', array(), false );
            wp_enqueue_script('video-js', 'https://vjs.zencdn.net/7.0.2/video.js', array(), true);
            wp_enqueue_script('jcd-scripts-project', get_template_directory_uri() . '/includes/js/project' . $debug . '.js', array('jquery'), '1.0', true);
        }

        wp_localize_script( 'jcd-scripts', 'jcd_config', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'homeurl' => home_url('/'),
            'phone_number' => get_field('contact_phone', 'option'),
            'contact_email' => get_field('contact_email', 'option'),
        ) );

    }
}
add_action( 'wp_enqueue_scripts', 'jcd_scripts_styles' );

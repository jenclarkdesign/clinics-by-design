<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<div class="wrap">
  <h2><?php _e( 'Settings', 'colabsthemes' ); ?></h2>
  
  <form action="" name="form" method="post">
  
    <h3><?php _e('Instagram client app credential', 'colabsthemes'); ?></h3>
  
    <p>Instagram Redirect URI: <code><?php echo $this->redirect_uri; ?></code></p>

    <table class="form-table">
      <tbody>           
        <tr>
          <th scope="row">
            <label for="client_id">Client ID</label>
          </th>
          <td>
            <input type="text" name="client_id" id="client_id" value="<?php echo $this->client_id; ?>" placeholder="Insert your Instagram Client ID here" class="regular-text">              
          </td>
        </tr>

        <tr>
          <th scope="row">
            <label for="client_secret">Client Secret</label>
          </th>
          <td>
            <input type="text" name="client_secret" id="client_secret" value="<?php echo $this->client_secret; ?>" placeholder="Insert your Instagram Client Secret here" class="regular-text">              
          </td>
        </tr>
      </tbody>
    </table>
    
    <?php wp_nonce_field( "wp_instagram_settings", "_wpnonce" ); ?>
    <p class="submit">
      <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e( 'Save Changes', 'colabsthemes' ); ?>">
    </p>
  </form>

  <hr>

  <?php 
  // Show import after user connect to instagram
  if( $this->exchange_code && $this->api()->is_connected() === true ) : ?>
    
    <p>You have connected you instagram account.</p>
    <p><a href="<?php echo admin_url('options-general.php?page=colabs_wp_instagram&action=logout'); ?>">Logout</a></p>

  <?php 
  // User hasn't connect to instagram, show the connect button
  else : ?>
    <h3>Connect Your Account</h3>  
    <p>Before importing instagram post, you need to connect to Instagram</p>
    <p></p><a href="#" id="auth-instagram">Connect to Instagram</a></p>
    <script type="text/javascript">
      jQuery(document).ready(function($){
        $('#auth-instagram').click(function(e){
          e.preventDefault();
          InstagramAuthWindow = window.open('<?php echo $this->authorization_url; ?>', 'Instagram Authorization', 'width=800,height=400');  
        });
      });
    </script> 
  
  <?php endif; ?>

</div>
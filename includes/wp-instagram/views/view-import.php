<?php
  if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

  // If there's no account information, display error
  if( ! isset( $this->account->id ) ){

    return;
  }

  // Define variables
?>

<div class="wrap">
  
  <h2><?php _e('Import', 'colabsthemes'); ?></h2>
  
  <?php if( isset( $_GET['action'] ) && $_GET['action'] == 'import' ) {

    // Import parameter
    $import_params = array(
      'count' => 5
    );

    // Append max id if there's any
    if( isset( $_GET['max_id'] ) ) {
      $import_params['max_id'] = $_GET['max_id'];
    }

    $import = $this->import()->import_items( $import_params );

    if( isset( $import['importing'] ) && !empty( $import['importing'] ) ){
    
      foreach ( $import['importing'] as $key => $item ) {

        echo '<li>';

        switch ( $item['status'] ) {
          case 'error':

            echo '<span style="font-weight: bold; color: red">' . __( "Error", "colabsthemes" ) . '</span>: ';

            _e( 'Cannot import Instagram media', 'colabsthemes' );
            
            break;
          
          case 'duplicate':

            echo '<span style="font-weight: bold; color: orange">' . __( "Duplicate", "colabsthemes" ) . '</span>: ';

            $permalink = get_permalink( $item['data']->ID );

            printf( __( 'Instagram media you want to import has been exist: %s', 'colabsthemes' ), "<a href='$permalink' target='_blank'>{$item['data']->post_title}</a>" );
            
            break;

          default:
            
            echo '<span style="font-weight: bold; color: green">' . __( "Success", "colabsthemes" ) . '</span>: ';

            $permalink = get_permalink( $item['data']->ID );

            printf( __( 'Instagram media imported: %s', 'colabsthemes' ), "<a href='$permalink' target='_blank'>{$item['data']->post_title}</a>" );
            
            break;
        }

        echo '</li>';

      }

      echo '</ul>';

      // If next set of media detected, continue to import
      if( isset( $import['pagination']->next_max_id ) && $import['pagination']->next_max_id != '' ){
        $next_import_url = admin_url( 'edit.php?post_type=instagram&page=colabs_instagram_import&action=import&max_id=' . $import['pagination']->next_max_id );
        ?>
          <script type="text/javascript">
            window.location = "<?php echo $next_import_url; ?>";
          </script>
          <p><?php _e( 'If your browser is not automatically moved to the next page, click here:', 'colabsthemes' ); ?> <a href="<?php echo $next_import_url; ?>"><?php _e( 'Next Page', 'colabsthemes' ); ?></a></p>
        <?php         
      }

      // Display all has been imported
      if( intval( $import['count'] ) < 10 ){
        
        echo '<p>';

        printf( __( 'All Instagram media has been imported! <a href="%s" target="_blank">View it here</a>.', 'colabsthemes' ), $this->import()->get_archive_url() );

        echo '</p>';

      }
    }

  } else {
    if( $this->api()->is_connected() ) {
      $this->import()->pre_import_message();
    } else {
      echo sprintf('You haven\'t connected to instagram. please connect your account from <a href="%s">here</a>', admin_url('edit.php?post_type=instagram&page=colabs_wp_instagram'));
    }
  } ?>

</div>
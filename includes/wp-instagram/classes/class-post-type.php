<?php
/**
 * Instagram Post Type
 */
class WP_Instagram_Post_Type {

  function __construct() {
    $this->register_post_type();

    // Customize columns
    add_filter( 'manage_instagram_posts_columns', array( $this, 'colabs_instagram_columns' ) );
    add_filter( 'manage_instagram_posts_custom_column', array( $this, 'colabs_instagram_column' ), 10, 3 );
    add_action( 'admin_print_styles-edit.php', array( $this, 'colabs_instagram_style' ) );
  }


  /**
   * Customize columns
   */
  function colabs_instagram_columns( $columns ) {
    $new_columns = array();
    $new_columns['cb'] = $columns['cb'];
    $new_columns['thumb'] = __( 'Thumb', 'colabsthemes' );

    unset( $columns['cb'] );

    return array_merge( $new_columns, $columns );
  }


  function colabs_instagram_column( $column ) {
    if( $column == 'thumb') {
      $image = '';
      $thumbnail_id = get_post_thumbnail_id( $post_id );

      if ($thumbnail_id)
        $image = wp_get_attachment_url( $thumbnail_id );

      echo '<img src="'. $image .'" style="max-width:40px">';
    }
  }


  function colabs_instagram_style() {
    $current_screen = get_current_screen();
    if( 'edit-instagram' == $current_screen->id ) {
      ?>
      <style>
        table.wp-list-table .column-thumb {
          width: 52px;
          text-align: center;
        }
      </style>
      <?php
    }
  }


  /**
   * Register instagram post type
   */
  function register_post_type() {
    // add custom posttype - slide
    $labels = array(
      'name'          => _x('Instagram', 'post type general name', 'colabsthemes'),
      'singular_name' => _x('Instagram', 'post type singular name', 'colabsthemes'),
      'add_new'       => _x('Add New', 'instagram', 'colabsthemes'),
      'add_new_item'  => __('Add New Instagram', 'colabsthemes'),
      'edit_item'     => __('Edit Instagram', 'colabsthemes'),
      'new_item'      => __('New Instagram', 'colabsthemes'),
      'view_item'     => __('View Instagram', 'colabsthemes'),
      'search_items'  => __('Search instagram', 'colabsthemes'),
      'not_found'     =>  __('No instagram found', 'colabsthemes'),
      'not_found_in_trash'    => __('No instagram found in Trash', 'colabsthemes'), 
      'parent_item_colon'     => ''
    );

    $args = array(
      'labels'            => $labels,
      'public'            => true,
      'publicly_queryable'=> true,
      'show_ui'           => true, 
      'query_var'         => true,
      'rewrite'           => true,
      'capability_type'   => 'post',
      'hierarchical'      => false,
      'has_archive'       => true,
      'menu_icon'         => 'dashicons-camera',
      'menu_position'     => null,
      'supports'          => array('title', 'thumbnail', 'editor', 'custom-fields'),
      // 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions', 'trackbacks', 'page-attributes', 'custom-fields' ),
    ); 

    register_post_type( 'instagram', $args);
  }

}

new WP_Instagram_Post_Type;
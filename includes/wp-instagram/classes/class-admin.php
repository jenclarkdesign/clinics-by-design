<?php
/**
 * Handle setting on admin
 */
class WP_Instagram_Admin {

  var $prefix;
  var $client_id;
  var $client_secret;
  var $authorization_url;
  var $exchange_code;
  var $setting_slug;
  var $redirect_uri;
  var $redirect_uri_encoded;
  protected $account;

  function __construct() {
    $this->prefix              = 'wp_instagram_';
    $this->client_id           = get_option( "{$this->prefix}client_id" );
    $this->client_secret       = get_option( "{$this->prefix}client_secret" );
    $this->account             = get_option( "{$this->prefix}account" );
    $this->exchange_code       = get_option( "{$this->prefix}exchange_code" );
    $this->access_token        = get_option( "{$this->prefix}access_token" );
    $this->setting_slug        = 'colabs_wp_instagram';
    $this->redirect_uri        = admin_url( 'admin-ajax.php?action=wp_instagram_redirect_uri' );
    $this->redirect_uri_encoded= urlencode( $this->redirect_uri );
    $this->authorization_url   = "https://api.instagram.com/oauth/authorize/?client_id={$this->client_id}&redirect_uri={$this->redirect_uri_encoded}&response_type=code";  

    if( isset( $_GET['page'] ) && $this->setting_slug == $_GET['page'] ){
      // Saving form submission
      add_action( 'init', array( $this, 'saving' ) );

      if( isset( $_GET['action'] ) && 'logout' == $_GET['action'] ) {
        add_action( 'init', array( $this, 'logout' ) );        
      }
    }

    // Register Settings Page
    add_action( 'admin_menu', array( $this, 'register_page' ), 20 );

    // Ajax callback for saving the access token
    add_action( 'wp_ajax_wp_instagram_redirect_uri', array( $this, 'redirect_callback' ) );
  }


  /**
   * Register submenu page
   */
  function register_page() {
    // Register instagram setting page
    add_submenu_page( 
      'options-general.php', // Parent slug
      __('Instagram Settings', 'jcd'), // Page title
      __('Instagram Settings', 'jcd'), // Menu title
      'manage_options', // Capability
      $this->setting_slug, // Slug
      array( $this, 'admin_setting_page' ) // Callback function
    );
  }


  /**
   * Render the admin page
   */
  function admin_setting_page() {
    include_once( WP_INSTAGRAM_DIR . 'views/view-settings.php' );
  }


  /**
   * Save settings
   */
  function saving() {
    if( isset( $_POST['client_id'] ) && 
        isset( $_POST['client_secret'] ) &&
        wp_verify_nonce( $_POST['_wpnonce'], 'wp_instagram_settings' ) )
    {
        update_option( "{$this->prefix}client_id", sanitize_text_field( $_POST['client_id'] ) );
        update_option( "{$this->prefix}client_secret", sanitize_text_field( $_POST['client_secret'] ) );
        wp_redirect( admin_url( 'options-general.php?page=colabs_wp_instagram&updated' ) );
    }
  }


  /**
   * Logout
   */
  function logout() {
    if( isset( $_REQUEST['action'] ) ) {
      delete_option( "{$this->prefix}account" );
      delete_option( "{$this->prefix}exchange_code" );
      delete_option( "{$this->prefix}access_token" );
      wp_redirect( admin_url('options-general.php?page=colabs_wp_instagram') );
      exit;
    }
  }


  /**
   * Ajax callback for instagram redirect URI
   */
  function redirect_callback() {
    if( isset( $_GET['code'] ) ){
      // Save the exchange_code
      $save_exchange_code = update_option( "{$this->prefix}exchange_code", esc_html( $_GET['code'] ) );
      $this->exchange_code = $_GET['code'];

      // Request access token
      $args = array(
        'timeout' => 60,
        'body' => array(
          'client_id' => $this->client_id,
          'client_secret' => $this->client_secret,
          'grant_type' => 'authorization_code',
          'redirect_uri' => $this->redirect_uri,
          'code' => $this->exchange_code,
        )
      );

      // Request access token to Instagram
      $request_access_token = wp_remote_post( "https://api.instagram.com/oauth/access_token", $args );

      if( isset( $request_access_token['body'] ) && $request_access_token['response']['code'] == '200' ){
        // Parse the body response
        $response = json_decode( $request_access_token['body'] );

        if( isset( $response->access_token ) ){
          // Save the access token
          $save_access_token = update_option( "{$this->prefix}access_token", esc_html( $response->access_token ) );

          // Save the user info
          $save_user = update_option( "{$this->prefix}account", $response->user );

          // Close the popup, then reload the parent page
          ?>
            <script type="text/javascript">
               opener.location.reload(true);
               self.close();
            </script>
          <?php
        } else {
          _e( "Fail to connect to your Instagram Account. Please try again", "jcd" );
        }
      } else {
        _e( "We cannot connect to Instagram. Please close this popup window then try again.", "jcd" );
      }
    }
    exit;
  }


  /**
   * Initiating api class as method
   * 
   */
  function api(){
    $api = new WP_Instagram_API( get_option( "{$this->prefix}access_token" ) );

    return $api;
  }


  /**
   * Initiating import class as method
   * 
   * @return obj WP_Instagram_Import
   */
  function import(){
    return new WP_Instagram_Import;
  }
}

new WP_Instagram_Admin;
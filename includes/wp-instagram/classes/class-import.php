<?php
/**
 * Handle instagram import process
 */
class WP_Instagram_Import {
  
  var $base_url;
  var $prefix;
  var $post_type;

  function __construct(){
    $this->base_url     = home_url();
    $this->prefix       = 'wp_instagram_';
    $this->post_type    = 'instagram';
  }


  /**
   * Initiating Instagram API
   * 
   * @return obj of Instagram API
   */
  function api(){
    $api = new WP_Instagram_API( get_option( "{$this->prefix}access_token" ) );

    return $api;
  }


  /**
   * Get account information
   * 
   * @return obj of connected account
   */
  function current_account(){
    $account = get_option( "{$this->prefix}account" );
    return $account;
  }


  /**
   * Pre-import message. Displaying before import message
   * 
   */
  function pre_import_message(){
    ?>
      
      <form action="<?php echo admin_url('edit.php?post_type=instagram&page=colabs_instagram_import&action=import'); ?>" name="form" method="post">  
        <p>Import instagram post into WordPress Instagram custom post type.</p>
        
        <?php wp_nonce_field( "wp_instagram_import", "_wpnonce" ); ?>
        <p class="submit">
          <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e( 'Import', 'colabsthemes' ); ?>">
        </p>
      </form>

    <?php
  }


  /**
   * Import items
   */
  function import_items( $user_media_param = array(), $self_feed = true ) {
    $user_media = $this->api()->user_media( $user_media_param );

    if( isset( $user_media->data ) && ! empty( $user_media->data ) ){

      $output = array(
        'status' => array(
          'duplicate' => 0,
          'success' => 0,
          'error' => 0,
        ),
        'pagination' => $user_media->pagination,
        'count' => count( $user_media->data ),
      );

      foreach( $user_media->data as $item ) {
        // Verify user based on user_id sameness (conditional)
        if( $self_feed && ( ! isset( $item->user->id ) || intval( $item->user->id ) != intval( $this->current_account()->id ) ) )
          continue;

        // Prevent duplication
        $existing = get_posts( array(
          'post_type' => $this->post_type,
          'meta_key' => '_instagram_id',
          'meta_value' => $item->id
        ) );

        if( !empty( $existing[0]->ID ) ){
          
          // Instagram media existed
          $output['importing'][$item->id] = array(
            'status' => 'duplicate',
            'permalink' => get_permalink( $existing[0]->ID ),
            'data' => $existing[0]
          );

          // Count status
          $output['status']['duplicate'] = intval( $output['status']['duplicate'] ) + 1;
        }

        else {

          // Import Instagram media
          $post_id = $this->import_item( $item );

          if( $post_id ) {
            $post = get_post( $post_id );

            // Instagram media imported
            $output['importing'][$item->id] = array(
              'status'  => 'success',
              'data'    => $post,
            );

            // Count status
            $output['status']['success'] = intval( $output['status']['success'] ) + 1;
          }

          else {

            // Instagram media imported
            $output['importing'][$item->id] = array(
              'status'  => 'error',
            );  

            // Count status
            $output['status']['error'] = intval( $output['status']['error'] ) + 1;

          }

        }

      }

      return $output;
    }

    else {
      return new WP_Error( 400, __( 'Cannot get Instagram media', 'colabsthemes' ) );
    }
  }


  /**
   * Import single item
   */
  function import_item( $item, $import_media = true, $post_status = 'publish' ) {

    /*$post_title = substr( $item->caption->text, 0, 30 );

    if( strlen( $item->caption->text ) > 30 ){
      $post_title .= "...";           
    }*/

    $post_title = date( 'j F Y H:i', ( intval( $item->created_time ) + ( wp_timezone_override_offset() * 60 * 60 ) ) );

    $link = $item->link;

    $link_exploded = explode( $link, '/' );

    if( isset( $link_exploded[4] ) ){
      $post_name = $link_exploded[4];           
    } else {
      $post_name = $post_title;
    }

    $post_content = '';

    $post_date = date( 'Y-m-d H:i:s', ( intval( $item->created_time ) + ( wp_timezone_override_offset() * 60 * 60 ) ) ); // Instagram gives timestamp in GMT hence it should be adjusted to user's preference

    $media_url = $item->images->standard_resolution->url;

    // Insert post
    $post_args = array(
      'post_name' => $post_name,
      'post_title'  => $post_title,
      'post_content'  => $post_content,
      'post_status' => $post_status,
      'post_date'   => $post_date,
      'post_type' => $this->post_type,
      'post_author' => get_current_user_id(),
    );

    $post_id = wp_insert_post( $post_args );

    if( $post_id ) {

      if( $import_media ) {
        $media_id = $this->upload_media( $media_url, $post_id );
        $featured_image = set_post_thumbnail( $post_id, $media_id );
      }

      // Insert appropriate post meta
      update_post_meta( $post_id, '_post_source', 'instagram' );
      update_post_meta( $post_id, '_instagram_id', $item->id );
      update_post_meta( $post_id, '_instagram_filter', $item->filter );
      update_post_meta( $post_id, '_instagram_user_id', $item->user->id );
      update_post_meta( $post_id, '_instagram_data', $item );

      return $post_id;
    }

    else {
      return new WP_Error( 400, __( 'Cannot import Instagram media', 'colabsthemes' ) );
    }

  }


  /**
   * Uploading given URL to media library
   * 
   * @param string media URL
   * 
   * @return mixed import id|WP_Error object
   */
  function upload_media( $media_url = false, $post_id = false ){

    // Load wp-load to prevent fatal error
    require_once( ABSPATH . "wp-admin/includes/file.php" );
    require_once( ABSPATH . "wp-admin/includes/media.php" );
    require_once( ABSPATH . "wp-admin/includes/image.php" );
    require_once( ABSPATH . "wp-admin/includes/post.php" );

    // Media URL have to be supplied
    if( ! $media_url ){
      return new WP_Error( 400, __( 'Media URL have to be given', 'colabsthemes' ) );
    }

    $import_media = download_url( $media_url );

    $import_media_array = array(
      'name' => basename( $media_url ),
      'tmp_name' => $import_media   
    );

    if( $post_id ){
      $import_media_array['post_id'] = $post_id;
    }

    // Check for download errors
    if ( is_wp_error( $import_media ) ) {
        @unlink( $import_media_array[ 'tmp_name' ] );
        return $import_media;
    }     

    $import_id = media_handle_sideload( $import_media_array, 0 );

    return $import_id;
  }


  /**
   * Get url of instagram archive page
   * Note: user can set post_type and category of imported item
   * 
   * @return string of URL
   */
  function get_archive_url(){
    if( 'post' == $this->post_type ){
      // display category archive if this post type is set to post
      return get_category_link( $this->post_category );
    } else {
      // otherwise, display post type archive   
      return get_post_type_archive_link( $this->post_type );  
    }
  }
}
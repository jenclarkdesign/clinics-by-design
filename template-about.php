<?php
/**
 * Template Name: About
 */

get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

    <article <?php post_class(); ?>>
        
        <?php get_template_part('partials/component/page-heading'); ?>

        <section class="wrapper block-section pt-0">
            <div class="grid">
                <div class="grid__item large--one-half image-block">
                    <div class="image-block__inner">
                        <?php if( $image = get_field( 'image' ) ) : ?>
                            <?php echo wp_get_attachment_image( $image['id'], 'large', null, array( 'class' => 'display-block' ) ); ?>
    
                            <?php if ( $caption = get_field('caption') ) : ?>
                                <div class="image-block__caption"><?php echo $caption; ?></div>
                            <?php endif; ?>
                        <?php endif; ?>	
                    </div>
                </div>
                <div class="grid__item large--one-half">
                    <div class="entry-content main-content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- .main-content -->
        
        <?php get_template_part( 'partials/component/our-team' ); ?>
        <?php get_template_part( 'partials/component/testimonials', 'content' ); ?>

    </article>

<?php endwhile; ?>

<?php get_footer(); ?>
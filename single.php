<?php get_header();?>

<?php get_template_part( 'partials/component/page-heading' ); ?>

<div class="main-content wrapper block-section pt-0">
    <div class="grid">
        <div class="grid__item large--eight-twelfths">
            <?php while( have_posts() ) : the_post(); ?>
                <article <?php post_class(); ?>>
                    <div class="blog-item__meta mb-20">
                        <?php the_author(); ?> <span class="sep">/</span>
                        <time datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date(); ?></time> <span class="sep">/</span>
                        Category <?php echo get_the_category_list(', '); ?> 
                    </div>

                    <div class="entry-content"><?php the_content(); ?></div>
                </article>
            <?php endwhile; ?>
        </div>
        <div class="grid__item large--four-twelfths">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div><!-- .main-content-section -->

<?php	get_footer(); ?>

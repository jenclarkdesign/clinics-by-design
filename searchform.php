<form class="search-form input-group" role="search" method="get" action="<?php echo home_url('/'); ?>">
    <label class="screen-reader-text"><?php _e('Search:', 'hallo'); ?></label>
    <input type="text" class="form-field form-field--text form-field--full" placeholder="<?php esc_attr_e('Search', 'hallo'); ?>" name="s" value="<?php echo get_search_query(); ?>">
    <span class="search-form__button input-group__button">
        <button class="button button--reset search-button" type="submit">
            <i class="icon-search" aria-hidden="true"></i>
            <span class="screen-reader-text"><?php _e('Search', 'hallo'); ?></span>
        </button>
    </span>
</form>
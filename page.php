<?php get_header();?>

<?php while ( have_posts() ) : the_post(); ?>

	<article <?php post_class(); ?>>
		
		<?php get_template_part('partials/component/page-heading'); ?>

		<section class="main-content wrapper block-section">
			<div class="grid">
				<div class="grid__item large--ten-twelfths push--large--one-twelfth">
					<div class="entry-content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
		<!-- .main-content -->

	</article>

<?php endwhile; ?>

<?php get_footer(); ?>

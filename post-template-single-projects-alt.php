<?php get_header(); ?>

<?php get_template_part('partials/component/page', 'heading'); ?>

<div class="main-section block-section pt-0">
    <div class="wrapper">
        <?php while (have_posts()) : the_post();
            global $post;
            $have_metadata = false;
            $metadata = array(
                'location' => 'Location', 
                'type' => 'Type', 
                'size' => 'Size', 
                'comments' => 'Comments',
            );
            foreach ($metadata as $meta_key => $meta_label) {
                if (get_field("meta_data_{$meta_key}")) {
                    $have_metadata = true;
                }
            }
        ?>
            <article <?php post_class('grid'); ?>>
                <?php if ($post->post_content || $have_metadata) : ?>
                    <div class="grid__item large--four-twelfths">
                        <div class="single-project__content">
                            <p><?php echo jcd_project_back_button(get_the_ID()); ?></p>

                            <?php if ($have_metadata) : ?>
                                <div class="single-project__meta mb-60">
                                    <?php foreach ($metadata as $meta_key => $meta_label) : 
                                        $value = get_field("meta_data_{$meta_key}");
                                        if ($value) : ?>
                                            <div class="single-project__meta__item mb-10">
                                                <div><strong><?php echo $meta_label; ?></strong></div>
                                                <div><?php echo $value; ?></div>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>

                            <div class="entry-content"><?php the_content(); ?></div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($post->post_content || $have_metadata) : ?>
                    <div class="grid__item large--eight-twelfths">
                <?php else : ?>
                    <div class="grid__item large--ten-twelfths push--large--one-twelfth">
                <?php endif; ?>
                    
                    <div class="project-gallery-grid grid">
                        <?php if (has_post_thumbnail()) : ?>
                            <div class="project-gallery-grid__item grid__item">
                                <a class="js-image-popup project-gallery-grid__item__link" href="<?php echo wp_get_attachment_image_url(get_post_thumbnail_id(), 'full'); ?>">
                                    <?php the_post_thumbnail('project-hero-image'); ?>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if ($gallery = get_field('gallery')) :
                            $counter = 1; ?>
                            <?php foreach ($gallery as $image) : ?>
                                <div class="project-gallery-grid__item grid__item one-half">
                                    <a class="js-image-popup project-gallery-grid__item__link project-gallery-grid__item__link--ratio" href="<?php echo wp_get_attachment_image_url($image['id'], 'full'); ?>">
                                        <div class="project-gallery-grid__background layer-cover layer-cover-bg" style="background-image: url('<?php echo wp_get_attachment_image_url($image['id'], 'blog-thumb'); ?>')"></div>
                                    </a>
                                </div>
                            <?php $counter++;
                            endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <?php if (!$post->post_content) : ?>
                        <p><?php echo jcd_project_back_button(get_the_ID()); ?></p>
                    <?php endif; ?>
                </div>
            </article>
        <?php endwhile; ?>
    </div>
</div><!-- .main-content-section -->

<?php get_footer(); ?>

<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head <?php language_attributes(); ?>>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <?php jcd_head(); ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class('antialiased'); ?>>
    
    <header class="sticky-bar">
        <div class="sticky-bar__inner">
            <div class="topbar-section">
                <div class="wrapper">

                    <div class="grid grid--middle topbar-section-grid">
                        <div class="grid__item eight-twelfths large--three-twelfths">
                            <div class="logo">
                                <a class="logo__link" href="<?php echo home_url(); ?>" title="<?php echo get_bloginfo('description'); ?>">
                                    <?php if (get_option('jcd_logo') != '') : ?>
                                        <img src="<?php echo esc_url(get_option('jcd_logo')); ?>" alt="<?php esc_attr_e(get_bloginfo('site_title')); ?>" class="logo__img">
                                    <?php else : ?>
                                        <?php echo get_bloginfo('site_title'); ?>
                                    <?php endif; ?>
                                </a>
                            </div>
                        </div><!--

                     --><div class="grid__item four-twelfths large--nine-twelfths text-align-right mobile-nav-grid">
                            <button class="menu-trigger">
                                <span class="menu-trigger__bar"></span>
                                <span class="menu-trigger__bar"></span>
                                <span class="menu-trigger__bar"></span>
                            </button>
                        </div><!--

                     --><div class="grid__item large--nine-twelfths main-nav-grid">
                            <?php
                            $phone_number = get_field('contact_phone', 'option');
                            $email = get_field('contact_email', 'option');
                            if ( $phone_number || $email ) : ?>
                                <ul class="contact-bar">
                                    <?php if ( $phone_number ) : ?>
                                        <li class="phone menu-contact">
                                            <a href="tel:<?php echo $phone_number; ?>">
                                                <i class="icon-phone"></i> <?php echo $phone_number; ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if ( $email ) : ?>
                                        <li class="menu-contact">
                                            <a href="mailto:<?php echo $email; ?>">
                                                <i class="icon-mail"></i> <?php echo $email; ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                            <nav class="main-nav text-align-right">
                                <ul>
                                    <?php
                                        wp_nav_menu(array(
                                            'theme_location' => 'main-menu',
                                            'container' => '',
                                            'container_class' => '',
                                            'menu_class' => '',
                                            'items_wrap' => '%3$s',
                                            'fallback_cb' => false,
                                            'item_spacing' => 'discard',
                                            'depth' => 1,
                                        ));
                                    ?>
                                </ul>
                            </nav><!-- .main-nav -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
    <!-- .sticky-bar -->

    <div class="outer-content-wrapper">

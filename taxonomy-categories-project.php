<?php 
get_header(); 
$term = get_queried_object();
$long_description = get_field('long_description', $term);
$image = get_field('large_image', $term);
?>

<?php get_template_part( 'partials/component/page-heading' ); ?>

<div class="main-section block-section pt-0 project-category-details">
    <div class="wrapper">
        <?php if ( $long_description || $image ) : ?>
            <div class="grid">
                <div class="grid__item large--eight-twelfths mb-30">
                    <?php if ($long_description) : ?>
                        <div class="entry-content">
                            <?php echo $long_description; ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="grid__item large--four-twelfths project-category-details__image">
                    <?php if ($image) : ?>
                        <?php echo wp_get_attachment_image( $image['id'], 'large' ); ?>
                    <?php endif; ?>
                </div>
            </div>

            <hr class="project-category-details__separator" />
        <?php endif; ?>

        <?php if ( have_posts() ) : ?>
            <h3 class="text-center block-section__title"><?php _e('Projects', 'jcd'); ?></h3>

            <div class="grid grid-uniform project-list js-mosaic-layout">
                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('partials/content/project-item'); ?>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- /.main-section -->

<?php get_footer(); ?>

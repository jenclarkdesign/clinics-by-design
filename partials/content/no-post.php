<?php if (is_search()) : ?>
	<p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
	<?php echo get_search_form(); ?>

<?php else : ?>

	<p>It looks like nothing was found at this location. Maybe try a search?</p>
	<?php echo get_search_form(); ?>

<?php endif; ?>
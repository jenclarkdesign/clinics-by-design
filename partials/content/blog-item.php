<?php
global $loop;

$post_classes = array('blog-item', 'block-item', 'antialiased', 'grid__item');
$columns = '1/3';

if (isset($loop['columns'])) {
    $columns = $loop['columns'];
}

switch ($columns) {
    case '1/2':
        $post_classes[] = 'large--one-half';
        $post_classes[] = 'medium--one-half';
        break;

    default:
        $post_classes[] = 'large--four-twelfths';
        $post_classes[] = 'medium--one-half';
        break;
}

if (isset($loop['bordered']) && $loop['bordered'] === true) {
    $post_classes[] = 'blog-item--bordered';
}

?>

<article <?php post_class(implode(' ', $post_classes)); ?>>
    <?php if (has_post_thumbnail()) : ?>
        <div class="blog-item__image">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('blog-thumb', array('class' => 'display-block')); ?>
            </a>
        </div>
    <?php endif; ?>

    <h2 class="blog-item__title">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </h2>

    <div class="blog-item__excerpt entry-content">
        <?php the_excerpt(); ?>
    </div>

    <div class="blog-item__meta">
        <?php the_author(); ?> <span class="sep">/</span>
        <time datetime="<?php echo get_the_time('c'); ?>"><?php echo get_the_date(); ?></time> <span class="sep">/</span>
        Category <?php echo get_the_category_list(', '); ?> 
    </div>
</article>
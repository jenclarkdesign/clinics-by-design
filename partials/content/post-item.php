<?php
global $loop;
$classes = array( 'post-item' );

if( isset( $loop['columns'] ) && $loop['columns'] == 4 ) {
	$classes[] = 'grid__item';
	$classes[] = 'large--one-quarter';
	$classes[] = 'medium--one-half';
}

?>
<article <?php post_class( implode(' ', $classes) ); ?>>
	<?php if( has_post_thumbnail() ) : ?>
		<?php 
				$image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
				$url_image = $image[0];
		?>		
		<div class="post-item__image" style="background-image: url(<?php echo $url_image ?>)">
			<img src="<?php echo $url_image; ?>" />
		</div>
	<?php endif; ?>
	<h2 class="entry-title post-item__title">
		<a class="post-item__link" href="<?php echo get_permalink();?>">
			<?php the_title();?>
		</a>
	</h2>

	<div class="post-item__content entry-content"><?php the_excerpt();?></div>
</article>

<?php
$color = '#444';
$terms =  wp_get_object_terms( get_the_ID(), 'categories-project' );
if ($terms) {
    $color = get_field('colour', $terms[0]);
}
?>
<a href="<?php echo the_permalink(); ?>" <?php post_class('js-mosaic-layout-item grid__item medium--six-twelfths large--four-twelfths project-item'); ?>>
    <div class="project-item__inner">
        <div class="project-item__overlay layer-cover">
            <h4 class="project-item__title"><?php the_title(); ?></h4>
            <div class="project-item__overlay-bg layer-cover" style="background-color: <?php echo $color; ?>"></div>
        </div>

        <?php if ( has_post_thumbnail() ) : ?>
            <div class="project-category-item__image layer-cover layer-cover-bg" style="background-image: url('<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'large' ); ?>')">
                <?php the_post_thumbnail( 'large' ); ?>
            </div>
        <?php endif; ?>
    </div>
</a>
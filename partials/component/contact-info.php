<?php 
$contact_data = array(
    'phone'  => __('Phone', 'jcd'),
    'fax'    => __('Fax', 'jcd'),
    'email'  => __('Email', 'jcd'),
    'address'=> __('Address', 'jcd'),
);

?>
<div class="contact-info-block">
    <h4 class="footer-title"><?php the_field('contact_title', 'option'); ?></h4>
    <ul>
        <?php foreach ( $contact_data as $key => $label ) : 
            $value = get_field( "contact_{$key}", 'option' ); 
            if ( $value ) : 
                if ( 'phone' === $key ) {
                    $value = sprintf('<a href="tel:%1$s">%1$s</a>', $value);
                } else if ( 'email' === $key ) {
                    $value = sprintf('<a href="mailto:%1$s">%1$s</a>', $value);
                }
                ?>
                <li class="contact-info-block__item contact-info-block__item--<?php echo esc_attr($key); ?>">
                    <?php printf("<span>%s:</span> %s", $label, $value); ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>

<?php
$title        = get_the_title();
$header_image = '';
$blog_id      = get_option('page_for_posts');
$color        = '#41B6A9';

if ( has_post_thumbnail() ) {
    $image        = wp_get_attachment_image_src(get_post_thumbnail_id(), 'page-heading-image');
    $header_image = $image[0];
}

if ( is_404() ) {
    $title = __('Page Not Found', 'jcd');

} else if ( is_tax('categories-project') ) {
    $term = get_queried_object(); 
    $title = $term->name;
    $color = get_field( 'colour', $term );

    if ($image = get_field('header_image', $term)) {
        $header_image = $image['url'];
    } else {
        if( $image = get_field( 'background_archive_categories_project', 'option' ) ){
            $header_image = $image['url'];
        }
    }


} else if ( is_singular( 'projects' ) ) {
    $terms =  wp_get_object_terms( get_the_ID(), 'categories-project' );
    if ( $terms ) {
        $color = get_field('colour', $terms[0]);
    }
    if( $image = get_field( 'background_single_project', 'option' ) ){
        $header_image = $image['url'];
    }

} else if ( is_home() ) {
    $title = get_the_title( $blog_id );

    if (has_post_thumbnail( $blog_id )) {
        $image = wp_get_attachment_image_src(get_post_thumbnail_id($blog_id), 'page-heading-image');
        $header_image = $image[0];
    }

} else if ( is_archive() ) {
    $title = get_the_archive_title();

} else if ( is_singular( 'post' ) ) {
    if (!has_post_thumbnail()) {
        if (has_post_thumbnail($blog_id)) {
            $image = wp_get_attachment_image_src(get_post_thumbnail_id($blog_id), 'page-heading-image');
            $header_image = $image[0];
        }
    }
}

?>

<header class="page-heading">
    <div class="wrapper">
        <h1 class="entry-title page-heading__title mt-0 mb-0">
            <?php echo $title; ?>
            <span class="page-heading__title-border" style="background-color: <?php echo $color; ?>"></span>
        </h1>
    </div>

    <?php if ($header_image != '') : ?>
        <div class="page-heading__background-image layer-cover-bg" style="background-image: url('<?php echo $header_image; ?>')"></div>
    <?php endif; ?>
</header>
<!-- .page-heading -->
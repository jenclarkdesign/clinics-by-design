<?php if( $testimonials = get_field('testimonials') ): ?>
    <div class="testimonial-section block-section">
        <div class="wrapper text-center">
            <div class="testimonial-slides js-testimonial-slides">
                <?php foreach ($testimonials as $testimonial) : ?>
                    <div class="testimonial-item">
                        <div class="grid">
                            <div class="grid__item large--eight-twelfths push--large--two-twelfths">
                                <div class="testimonial-item__description"><?php echo $testimonial['description']; ?></div>
                                <h4 class="testimonial-item__name"><?php echo $testimonial['name']; ?></h4>
                                <div class="testimonial-item__position"><?php echo $testimonial['position']; ?></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif;
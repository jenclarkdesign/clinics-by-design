<?php if( $our_team = get_field( 'our_team' ) ): ?>
    <div class="team-section block-section">
        <div class="wrapper">
            <h3 class="block-section__title block-section__title--smaller text-center"><?php _e( 'Our Team', 'jcd' ); ?></h3>
            
            <div class="team-list-wrapper">
                <div class="team-list">
                    <div class="grid grid-uniform">
                        <?php foreach( $our_team as $index => $team ): ?>
                            <div class="grid__item medium--one-half large--three-twelfths team-item">
                                <a href="#team-detail-<?php echo $index; ?>" class="team-item__link display-block text-center">
                                    <?php if( $image = $team['image']) : ?>
                                        <div class="team-item__image">
                                            <?php echo wp_get_attachment_image( $image['id'], 'thumbnail', null, array( 'class' => 'display-block' ) ); ?>
                                        </div>
                                    <?php endif; ?>
                                    <h4 class="team-item__name"><?php echo $team['name'];?></h4>
                                    <div class="team-item__position text-uppercase"><?php echo $team['position']; ?></div>
                                    <div class="team-item__description"><?php echo $team['description']; ?></div>
                                </a>

                                <div id="team-detail-<?php echo $index; ?>" class="mfp-hide team-popup zoom-anim-dialog">
                                    <div class="zoom-anim-dialog__inner team-popup__inner clearfix">
                                        <?php if ($image = $team['image']) : ?>
                                            <div class="team-popup__image team-item__image">
                                                <?php echo wp_get_attachment_image($image['id'], 'staff-photo-large', null, array('class' => 'display-block')); ?>
                                            </div>
                                        <?php endif; ?>
    
                                        <div class="team-popup__content">
                                            <h4 class="team-item__name team-popup__name"><?php echo $team['name']; ?></h4>
                                            <div class="team-item__position team-popup__position"><?php echo $team['position']; ?></div>
    
                                            <?php if ($team['long_description']) : ?>
                                                <div class="team-popup__long-description entry-content"><?php echo $team['long_description']; ?></div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;
<?php
// ACF Flexible Content
if( have_rows( 'content_builder' ) ) : ?>
    <div class="flexible-content">
        <?php while( have_rows( 'content_builder' ) ) {
            the_row();
            
            switch ( get_row_layout() ) {
                case 'hero_section':
                    get_template_part('partials/layout/hero-section');
                break;
                
                case 'hero_text':
                    get_template_part('partials/layout/hero-text');
                break;

                case 'unit_section':
                    get_template_part('partials/layout/unit-section');
                break;

                case 'projects_section':
                    get_template_part('partials/layout/projects-section');
                break;

                case 'about_section':
                    get_template_part('partials/layout/about-section');
                break;

                case 'instagram_section':
                    get_template_part('partials/layout/instagram-section');
                break;

                case 'text_banner':
                    get_template_part('partials/layout/text-banner');
                break;

                case 'post_section':
                    get_template_part('partials/layout/post-section');
                break;

            }
        } ?>
    </div>
    <!-- /.flexible-content -->
<?php endif; ?>
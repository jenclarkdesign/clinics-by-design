<?php if( $services = get_field( 'services' ) ) : ?>
    <div class="service-list">
        <?php foreach ( $services as $value ): ?>
            <?php 
            $item_class = '';
            $image = $value['image'];
            if (!$image) {
                $item_class = 'service-item--no-image';
            }
            ?>
            <div class="service-item clearfix <?php echo $item_class; ?>">
                <div class="service-item__image">
                    <?php if( $image ): ?>
                        <?php echo wp_get_attachment_image( $image['id'], 'thumbnail' ); ?>
                    <?php endif; ?>
                </div>
                
                <div class="service-item__content">
                    <h4 class="service-item__title"><?php echo $value['title']; ?></h4>
                    <div class="service-item__description"><?php echo $value['description']; ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif;
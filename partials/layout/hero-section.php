<?php
$images = get_sub_field( 'images' );
$title = get_sub_field( 'title' );
$sub_title = get_sub_field( 'sub_title' );

if( $images ) : ?>
    <div class="hero-section">
        <div class="hero-section__image">
            <div class="hero-section__image-bg hero-section__slides js-hero-slides">
                <?php foreach ($images as $image) : ?>
                    <div class="hero-section__slide-item">
                        <div class="hero-section__slide-item__image layer-cover-bg" style="background-image: url('<?php echo $image['sizes']['hero-image']; ?>')">
                            <?php echo wp_get_attachment_image( $image['id'], 'hero-image' ); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="hero-section__image-overlay layer-cover-bg layer-cover"></div>
        </div>

        <?php if ($title || $sub_title) : ?>
            <div class="hero-section__text-content layer-cover">
                <div class="wrapper hero-section__text-wrapper">
                    <div class="hero-section__text">
                        <?php if ($title) : ?>
                            <h1 class="hero-section__title-large"><?php echo $title; ?></h1>
                        <?php endif; ?>
    
                        <?php if ($sub_title) : ?>
                            <h2 class="hero-section__subtitle"><?php echo $sub_title; ?></h2>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <!-- /.hero-section -->
<?php endif; 

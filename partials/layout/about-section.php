<?php
$image       = get_sub_field( 'image' );
$description = get_sub_field( 'description' );
$button_text = get_sub_field( 'button_text' );
$button_url  = get_sub_field( 'button_url' );

if ( $description ): ?>
    <section class="about-section">	
        <div class="grid grid--full grid--rev">
            <div class="grid__item about-section__item large--one-half">
                <?php if ( $image ) : ?>
                    <div class="about-section__image layer-cover-bg" style="background-image: url('<?php echo $image['sizes']['large']; ?>')">
                        <?php echo wp_get_attachment_image( $image['id'], 'large', '' , array('class' => 'display-block') ); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="grid__item about-section__item large--one-half">
                <div class="about-section__inner wrapper block-section">
                    <div class="about-section__description entry-content mb-30"><?php echo $description; ?></div>
                    <?php if ($button_text && $button_url) : ?>
                        <a href="<?php echo $button_url; ?>" class="button button--primary button--outline about-section__button block-section__button"><?php echo $button_text; ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
endif;
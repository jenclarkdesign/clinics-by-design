<?php
$title       = get_sub_field( 'title' );
$description = get_sub_field( 'description' );
$button_text = get_sub_field( 'button_text' );
$button_url  = get_sub_field( 'button_url' );

if( $title || $description ) : ?>
    <div class="hero-section hero-section--text-only">
        <div class="hero-section__inner wrapper block-section">
            <div class="grid">
                <div class="grid__item large--four-twelfths">
                    <h1 class="hero-section__title h2 mb-20"><?php echo $title; ?></h1>
                </div>
                <div class="grid__item large--eight-twelfths">
                    <div class="hero-section__content">
                        <div class="hero-section__description entry-content mb-30"><?php echo $description; ?></div>
    
                        <?php if ($button_text && $button_url) : ?>
                            <a href="<?php echo $button_url; ?>" class="button button--primary button--outline hero-section__button block-section__button"><?php echo $button_text; ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.hero-section -->
<?php endif; 

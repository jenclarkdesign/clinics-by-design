<?php
$title          = get_sub_field( 'title' ); 
$posts_per_page = ( get_sub_field( 'posts_per_page' ) ) ? get_sub_field( 'posts_per_page') : 12;

// The final project data
$collections = array();

// Get the project categories first to be counted
$terms = get_terms( array( 
    'taxonomy'   => 'categories-project',
    'hide_empty' => true,
) );

if( empty( $terms ) ){
    return false;
}

// Count the project categories and determine how many project to show per category
$count_term = count($terms);
$count_per_term = floor ( $posts_per_page / $count_term );

// Loop each project category and get the project for each category
foreach ( $terms as $term ) {
    $array_project = get_project_by_categories( $term->term_id, $count_per_term );
    $collections = array_merge( $collections , $array_project );
}

if ( count( $collections ) != $posts_per_page ) {
    $count_addition  = $posts_per_page - count($collections);
    $array_project_addition = get_project_by_categories_addition( $collections , $count_addition );
    $collections = array_merge($collections, $array_project_addition );
}

$args = array(
    'post_type'      => 'projects',
    'post_status'    => array( 'publish' ),
    'posts__in'      => $collections,
    'posts_per_page' => $posts_per_page,
);
$projects = new WP_Query( $args );

if ( $projects->have_posts() ) : ?>
    <div class="project-section block-section">
        <div class="wrapper">
            <h3 class="text-center block-section__title"><?php echo $title; ?></h3>

            <ul class="project-category-list js-mosaic-layout-filter clearfix">
                <li class="project-category-item all is-active">
                    <button data-filter="*"><?php _e('All', 'jcd'); ?></button>
                </li>
                <?php foreach ( $terms as $term ) : 
                    $color = ( get_field( 'colour', $term ) ) ? get_field( 'colour', $term ) : '#222';?>
                    <li>
                        <button data-filter=".categories-project-<?php echo $term->slug; ?>" style="color: <?php echo $color; ?>">
                            <?php echo $term->name; ?>
                        </button>
                    </li>
                <?php endforeach; ?>
            </ul><!-- .project-category-list -->

            <div class="grid grid-uniform project-list js-mosaic-layout">
                <?php while ( $projects->have_posts() ) : $projects->the_post(); ?>
                    <?php get_template_part( 'partials/content/project-item' ); ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php wp_reset_postdata(); endif; ?>

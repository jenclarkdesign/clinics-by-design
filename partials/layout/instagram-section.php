<?php

$title           = get_sub_field( 'title' );
$instagram_token = get_option('wp_instagram_access_token');
$instagram_count = get_sub_field('posts_per_page');


if( $instagram_token ) :
    $instagram_api = new WP_Instagram_API( $instagram_token );
    $user_media = $instagram_api->user_media(array(
        'user_id' => 'self',
        'count'   => $instagram_count,
    ));

    $user_url = 'http://instagram.com/';
    $user = $instagram_api->get( "{$instagram_api->endpoint}users/self?access_token={$instagram_api->access_token}" );
    if( $user && $user->meta->code == 200 ) {
        $user_url .= $user->data->username;
    }

    if( $user_media && $user_media->meta->code == 200 ) : ?>

        <div class="instagram-section block-section">
            <div class="wrapper text-center">
                <h3 class="text-center instagram-section__title"><?php echo $title; ?></h3>
                <div class="grid grid-uniform instagram-item-list">
                    <?php foreach ($user_media->data as $data) : ?>
                        <div class="grid__item instagram-item one-half block-square medium--four-twelfths large--two-twelfths">
                            <div class="block-square__inner">
                                <a target="_blank" href="<?php echo esc_attr($data->link); ?>" class="block-square__center display-block">
                                    <img class="display-block" src="<?php echo $data->images->low_resolution->url; ?>" alt="<?php echo $data->caption->text; ?>">
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <a target="_blank" href="<?php echo $user_url; ?>" class="mt-30 instagram-section__button block-section__button button button--primary button--outline">Follow Us on Instagram</a>
            </div>
        </div>

        

    <?php endif;
endif;
<?php
$text       = get_sub_field('text');
$image      = get_sub_field('background_image');
$bg_color   = get_sub_field('background_color');
$text_color = get_sub_field('text_color');

if ( $text ) : ?>
    <div class="text-banner block-section text-banner--<?php echo $text_color; ?>">
        <div class="wrapper text-banner__wrapper text-align-center">
            <h3 class="h1 text-banner__title"><?php echo $text; ?></h3>
        </div>

        <?php if ( $bg_color ) : ?>
            <div class="text-banner__color layer-cover layer-cover-bg" style="background-color: <?php echo $bg_color; ?>"></div>
        <?php endif; ?>

        <?php if ( $image ) : ?>
            <div class="text-banner__image layer-cover layer-cover-bg" style="background-image: url('<?php echo $image['sizes']['hero-image']; ?>')"></div>
        <?php endif; ?>
    </div>
    <!-- /.text-banner block-section -->
<?php endif; ?>

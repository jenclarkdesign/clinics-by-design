<?php 
$unit = get_sub_field( 'unit_categories' );

if( $unit ): ?>
    <div class="project-categories-section block-section">
        <div class="wrapper project-categories-list-wrapper">
            <div class="project-categories-list">
                <div class="grid grid-uniform">
                    <?php foreach( $unit as $term ): ?>
                        <?php $color = ( get_field( 'colour', $term ) ) ? get_field( 'colour', $term ) : '#41B6A9'; ?>
                        <div class="grid__item medium--one-half large--three-twelfths text-align-center project-categories-item">
                            <?php if( $image = get_field( 'image', $term ) ): ?>
                                <div class="project-categories-item__image-wrapper" style="background-color: <?php echo $color; ?>">
                                    <a class="project-categories-item__image" href="<?php echo get_term_link( $term->term_id );?>">
                                        <?php echo wp_get_attachment_image( $image['id'], 'full' ); ?>
                                    </a>
                                </div>
                            <?php endif; ?>
                            
                            <h3 class="project-categories-item__title h3"><?php echo $term->name; ?></h3>
                            <div class="project-categories-item__description"><?php echo $term->description; ?></div>
                            <div class="project-categories-item__button">
                                <a class="button button--fullwidth button--outline button--custom-<?php echo $term->term_id; ?>" href="<?php echo get_term_link( $term->term_id );?>"><?php _e( 'Learn More', 'jcd' ); ?></a>
                            </div>
                            <style>
                                .button--custom-<?php echo $term->term_id; ?> {
                                    background-color: transparent;
                                    border-color: <?php echo $color; ?>;
                                    color: <?php echo $color; ?>;
                                }

                                .button--custom-<?php echo $term->term_id; ?>:hover {
                                    background-color: <?php echo $color; ?>;
                                    color: #fff;
                                }
                            </style>
                        </div >
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif;
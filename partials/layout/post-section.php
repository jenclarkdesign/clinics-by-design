<?php
$title = get_sub_field('title');
$posts_per_page = get_sub_field('posts_per_page');
$post_query = new WP_Query(array(
    'post_type' => 'post',
    'ignore_sticky_posts' => true,
    'posts_per_page' => $posts_per_page,
    'no_found_rows' => true,
));

if ($post_query->have_posts()) : ?>
    <div class="post-section block-section">
        <div class="wrapper">
            <?php if ($title): ?>
                <h3 class="text-center block-section__title"><?php echo $title; ?></h3>
            <?php endif; ?>

            <div class="blog-list-wrapper">
                <div class="blog-list">
                    <div class="grid grid-uniform">
                        <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                            <?php 
                            global $loop;
                            $loop['columns'] = '1/3';
                            get_template_part( 'partials/content/blog-item' ); 
                            ?>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>

            <div class="text-align-center">
                <a href="<?php echo get_post_type_archive_link('post'); ?>" class="mt-60 instagram-section__button block-section__button button button--primary button--outline">
                    <?php _e('View More', 'jcd'); ?>
                </a>
            </div>
        </div>
    </div>
<?php endif; ?>
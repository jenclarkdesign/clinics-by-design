import $ from "jquery";
import imagesLoaded from "imagesloaded";
import Isotope from "isotope-layout";
import { $window } from "../global";

const List = document.querySelector(".js-mosaic-layout");
const Filter = $('.js-mosaic-layout-filter');

if ( List ) {
  const Layout = new Isotope( List, {
    percentPosition: true,
    itemSelector: '.js-mosaic-layout-item'
  });
  
  // Re-layout Isotope after images loaded
  const ImageLoad = imagesLoaded( List );
  ImageLoad.on( 'progress', (instance, img) => {
    Layout.layout();
  });

  // Filter Function
  Filter.on( 'click', 'button', (e) => {
    e.preventDefault();

    const $button = $(e.currentTarget),
        filterValue = $button.data('filter');

    $button.parent().addClass('is-active').siblings().removeClass('is-active');

    Layout.arrange({
      filter: filterValue
    });
  });
}
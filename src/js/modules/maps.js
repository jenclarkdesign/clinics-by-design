import $ from "jquery";

$.fn.acfMaps = function() {
  var maps = {
    /**
     * Render Map
     */
    render: function($el) {
      var _self = this,
        $markers = $el.find(".marker");

      // vars
      var args = {
        zoom: 16,
        // scrollwheel: false,
        center: new google.maps.LatLng(0, 0),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [
          {
            featureType: "water",
            elementType: "geometry",
            stylers: [
              {
                color: "#e9e9e9"
              },
              {
                lightness: 17
              }
            ]
          },
          {
            featureType: "landscape",
            elementType: "geometry",
            stylers: [
              {
                color: "#f5f5f5"
              },
              {
                lightness: 20
              }
            ]
          },
          {
            featureType: "road.highway",
            elementType: "geometry.fill",
            stylers: [
              {
                color: "#ffffff"
              },
              {
                lightness: 17
              }
            ]
          },
          {
            featureType: "road.highway",
            elementType: "geometry.stroke",
            stylers: [
              {
                color: "#ffffff"
              },
              {
                lightness: 29
              },
              {
                weight: 0.2
              }
            ]
          },
          {
            featureType: "road.arterial",
            elementType: "geometry",
            stylers: [
              {
                color: "#ffffff"
              },
              {
                lightness: 18
              }
            ]
          },
          {
            featureType: "road.local",
            elementType: "geometry",
            stylers: [
              {
                color: "#ffffff"
              },
              {
                lightness: 16
              }
            ]
          },
          {
            featureType: "poi",
            elementType: "geometry",
            stylers: [
              {
                color: "#f5f5f5"
              },
              {
                lightness: 21
              }
            ]
          },
          {
            featureType: "poi.park",
            elementType: "geometry",
            stylers: [
              {
                color: "#dedede"
              },
              {
                lightness: 21
              }
            ]
          },
          {
            elementType: "labels.text.stroke",
            stylers: [
              {
                visibility: "on"
              },
              {
                color: "#ffffff"
              },
              {
                lightness: 16
              }
            ]
          },
          {
            elementType: "labels.text.fill",
            stylers: [
              {
                saturation: 36
              },
              {
                color: "#333333"
              },
              {
                lightness: 40
              }
            ]
          },
          {
            elementType: "labels.icon",
            stylers: [
              {
                visibility: "off"
              }
            ]
          },
          {
            featureType: "transit",
            elementType: "geometry",
            stylers: [
              {
                color: "#f2f2f2"
              },
              {
                lightness: 19
              }
            ]
          },
          {
            featureType: "administrative",
            elementType: "geometry.fill",
            stylers: [
              {
                color: "#fefefe"
              },
              {
                lightness: 20
              }
            ]
          },
          {
            featureType: "administrative",
            elementType: "geometry.stroke",
            stylers: [
              {
                color: "#fefefe"
              },
              {
                lightness: 17
              },
              {
                weight: 1.2
              }
            ]
          }
        ]
      };

      // create map
      var map = new google.maps.Map($el[0], args);

      // add a markers reference
      map.markers = [];

      // add markers
      $markers.each(function() {
        _self.addMarker($(this), map);
      });

      // center map
      _self.centerMap(map);

      // _self.eventBinding( map, $el );
    },

    /**
     * Event Binding
     */
    eventBinding: function(map, $el) {
      // This will enable zoom scroll when user interact with the map
      google.maps.event.addListener(map, "mousedown", function() {
        map.setOptions({ scrollwheel: true });
      });

      // Disable zoom when user no longer interact with the map
      $("body").on("mousedown", function(event) {
        var clickedInsideMap = $(event.target).parents($el.selector).length > 0;

        if (!clickedInsideMap) {
          map.setOptions({ scrollwheel: false });
        }
      });

      $(window).scroll(function() {
        map.setOptions({ scrollwheel: false });
      });
    },

    /**
     * Add Marker
     */
    addMarker: function($marker, map) {
      // var
      var latlng = new google.maps.LatLng(
        $marker.data("lat"),
        $marker.data("lng")
      );

      // create marker
      var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: {
          path:
            "M11.348 15.52c-2.138 0-3.878-1.788-3.878-3.987 0-2.2 1.74-3.988 3.878-3.988 2.14 0 3.88 1.79 3.88 3.988 0 2.2-1.74 3.988-3.88 3.988m0-11.02c-3.816 0-6.92 3.156-6.92 7.033 0 3.877 3.104 7.032 6.92 7.032 3.817 0 6.923-3.155 6.923-7.032 0-3.877-3.105-7.032-6.922-7.032m0 28.21c-1.177-1.303-3.034-3.477-4.883-6.1C2.548 21.056.478 16.076.478 12.21.478 1.324 8.798.478 11.348.478c10.085 0 10.87 8.98 10.87 11.732 0 7.655-7.79 17.073-10.87 20.5",
          scale: 1,
          strokeWeight: 0,
          strokeColor: "#BA197A",
          strokeOpacity: 0,
          fillColor: "#BA197A",
          fillOpacity: 1
        }
      });

      // add to array
      map.markers.push(marker);

      // if marker contains HTML, add it to an infoWindow
      if ($marker.html()) {
        // create info window
        var infowindow = new google.maps.InfoWindow({
          content: $marker.html()
        });

        // show info window when marker is clicked
        google.maps.event.addListener(marker, "click", function() {
          infowindow.open(map, marker);
        });
      }
    },

    /**
     * Center Map
     */
    centerMap: function(map) {
      // vars
      var bounds = new google.maps.LatLngBounds();

      // loop through all markers and create bounds
      $.each(map.markers, function(i, marker) {
        var latlng = new google.maps.LatLng(
          marker.position.lat(),
          marker.position.lng()
        );
        bounds.extend(latlng);
      });

      // only 1 marker?
      if (map.markers.length == 1) {
        // set center of map
        map.setCenter(bounds.getCenter());
        map.setZoom(16);
      } else {
        // fit to bounds
        map.fitBounds(bounds);
      }
    },

    /**
     * Enable or disable map zooming with mouse wheel
     * http://bdadam.com/blog/simple-usability-trick-for-google-maps.html
     */
    enableScrollingWithMouseWheel: function() {
      this.getInstance().setOptions({ scrollwheel: true });
    },
    disableScrollingWithMouseWheel: function() {
      this.getInstance().setOptions({ scrollwheel: false });
    }
  };

  this.each(function() {
    maps.render($(this));
  });
};

$(".acf-map").acfMaps();

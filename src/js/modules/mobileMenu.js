import $ from "jquery";
import "jquery.mmenu";
import "jquery.mmenu/dist/wrappers/wordpress/jquery.mmenu.wordpress";

const RightPanel = $("#right-panel");
const navbarsContent = [];

if ( jcd_config.phone_number ) {
  navbarsContent.push(`<a class="icon-phone" href="tel:${jcd_config.phone_number}"></a>`);
}

if ( jcd_config.contact_email ) {
  navbarsContent.push(`<a class="icon-mail" href="mailto:${jcd_config.contact_email}"></a>`);
}

RightPanel.mmenu(
  {
    offCanvas: {
      position: "right",
      zposition: "front"
    },
    extensions: ["pagedim-black", "border-full"],
    navbars: [
      {
        position: "bottom",
        content: navbarsContent,
      }
    ]
  },
  {
    offCanvas: {
      pageSelector: ".outer-content-wrapper"
    }
  }
);

// Manually trigger the menu
$(".menu-trigger").on("click", () => {
  RightPanel.data("mmenu").open();
});

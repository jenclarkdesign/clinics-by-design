import $ from "jquery";
import debounce from "lodash.debounce";
import fitvids from "fitvids";
// import requestAnimationFrame from "requestAnimationFrame";
import "jquery-match-height";
import "slick-carousel";
import "magnific-popup";

import { $body, $window } from "../global";
import { hasAdminBar } from "../services/helper";

/**
 * If browser doesn't support svg as img tag, replace with PNG file
 */
if (!Modernizr.svgasimg) {
  var $logo = $(".logo__img[src$='.svg']");
  $logo.attr("src", $logo.attr('src').replace('.svg', '.png'));
}


/**
 * Match Height
 */
$('.project-categories-item__description').matchHeight();
$('.about-section__item').matchHeight();


/**
 * Responsive Video
 */
fitvids('.entry-content');


/**
 * Set ATF Section height
 */
const $headerSection = $('.topbar-section');
const $atfSection = $('.hero-section__image');
function setATFHeight() {
  $atfSection.removeAttr('style');

  // if ( $window.width() >= 769 ) {
    const windowHeight = $window.height();
    const headerHeight = $headerSection.outerHeight(true);
    const adminbarHeight = hasAdminBar() ? 32 : 0;
    let heroHeight = windowHeight - adminbarHeight - headerHeight;

    // Reduce the height by 50%
    if ( $window.width() >= 768 ) {
      let reduceBy = heroHeight * 32.5 / 100;
      heroHeight -= reduceBy;
    }
  
    $atfSection.height(heroHeight);
  // }
}

if ($atfSection.length > 0) {
  setATFHeight();
  $window.on('resize', debounce(setATFHeight, 200));
}



/**
 * Testimonial Slides
 */
if ( $('.js-testimonial-slides').length ) {
  $('.js-testimonial-slides').slick({
    dots: true,
    infinite: false,
    arrows: false,
  });
}


/**
 * Single Project Gallery
 */
if ( $('.js-gallery-slides') ) {
  const $gallery = $('.js-gallery-slides').slick({
    dots: false,
    arrows: false,
    infinite: false,
    fade: true,
    adaptiveHeight: true,
  });

  $('.js-gallery-thumbs').on('click', 'button', (e) => {
    e.preventDefault();
    var $button = $(e.currentTarget);
    $button.addClass('active').siblings().removeClass('active');
    $gallery.slick('slickGoTo', $button.data('index'));
    $('body, html').animate({
      scrollTop: $gallery.offset().top - $('.topbar-section').outerHeight(true),
    });
  });
}


/**
 * Hero Section Slide
 */
if ($('.js-hero-slides').length) {
  $(".js-hero-slides").slick({
    fade: true,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: false,
    dots: false,
    arrows: false,
  });
}

/**
 * Modal
 */
$('.js-image-popup').magnificPopup({
  type: 'image',
  mainClass: 'mfp-zoom-anim',
  gallery: {
    enabled: true
  }
});

$('.team-item__link').magnificPopup({
  type: 'inline',
  mainClass: 'mfp-zoom-anim',
  removalDelay: 500,
  preloader: false,
  alignTop: true,
  closeBtnInside: true,
});
import $ from "jquery";
import "magnific-popup";

$(".js-video-popup").magnificPopup({
  type: "inline",
  mainClass: "mfp-zoom-anim",
  removalDelay: 500,
  preloader: false,
  closeBtnInside: true,
  gallery: {
    enabled: true
  }
});
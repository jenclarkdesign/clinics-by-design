<?php
/**
 * Template Name: What We Do
 */

get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

    <article <?php post_class(); ?>>
        
        <?php get_template_part('partials/component/page-heading'); ?>

        <section class="main-section wrapper block-section pt-0">
            <div class="grid">
                <div class="grid__item large--one-half mb-60">
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                </div>

                <div class="grid__item large--five-twelfths push--large--one-twelfth">
                    <?php get_template_part( 'partials/component/services-content' ); ?>
                </div>
            </div>
        </section>
    
    </article>

<?php endwhile; ?>

<?php get_footer(); ?>
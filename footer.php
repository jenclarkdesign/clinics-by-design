        <footer class="footer-section">
            <div class="wrapper">
                <div class="grid mb-60">
                    <div class="grid__item large--six-twelfths footer-section__grid-item">
                        <?php get_template_part( 'partials/component/contact', 'info'); ?>
                    </div>
                    <div class="grid__item large--three-twelfths footer-section__grid-item">
                        <nav class="footer-nav">
                            <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer-menu',
                                    'container' => '',
                                    'container_class' => '',
                                    'menu_class' => '',
                                    'depth' => 1,
                                    'item_spacing' => 'discard',
                                ));
                            ?>
                        </nav>
                    </div>
                    <div class="grid__item large--three-twelfths footer-section__grid-item">
                        <h4 class="footer-title"><?php _e( 'Social', 'jcd' ); ?></h4>
                        <?php get_template_part('partials/component/social-links'); ?>
                    </div>
                </div>

                <div class="grid">
                    <div class="grid__item large--one-half copyrights">
                        <?php jcd_copyrights(); ?>
                    </div>
                    <div class="grid__item large--one-half copyrights copyrights--right">
                        <?php if ( $footer_text = get_option('jcd_footer_text') ) : ?>
                            <?php echo $footer_text; ?>
                        <?php else : ?>
                            Clinics by Design is part of <a href="http://www.coopergroup.com.au/" target="_blank">Cooper Group</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.footer-section -->
    
    </div>
    <!-- /.outer-content-wrapper -->


    <div id="right-panel" class="side-panel">
        <div class="mm-panels">
            <ul>
                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'main-menu',
                        'container' => '',
                        'container_class' => '',
                        'menu_class' => '',
                        'items_wrap' => '%3$s',
                        'fallback_cb' => false,
                        'item_spacing' => 'discard',
                        'depth' => 1,
                    ));
                ?>
            </ul>
        </div>
    </div>

    <?php wp_footer(); ?>

</body>
</html>

<?php
/**
 * Include Advance Custom Fields Scripts
 */
add_filter( 'acf/settings/path', 'jcd_acf_settings_path' );
function jcd_acf_settings_path( $path ) {
	$path = get_template_directory() . '/functions/acf/';
	return $path;
}

add_filter( 'acf/settings/dir', 'jcd_acf_settings_dir' );
function jcd_acf_settings_dir( $dir ) {
	$dir = get_template_directory_uri() . '/functions/acf/';
	return $dir;
}

// add_filter('acf/settings/show_admin', '__return_false');

include_once( get_template_directory() . '/functions/acf/acf.php' );
// include_once( get_template_directory() . '/functions/acf/addons/acf-repeater/acf-repeater.php' );

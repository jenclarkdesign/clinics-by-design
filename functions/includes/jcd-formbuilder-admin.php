<?php

/**
 * Add a contact form button to the post composition screen
 */
add_action( 'media_buttons', 'jcd_media_button', 999 );
function jcd_media_button( ) {
	global $post_ID, $temp_ID;
	$iframe_post_id = (int) (0 == $post_ID ? $temp_ID : $post_ID);
	$title = esc_attr( __( 'Add a custom form', 'jcd' ) );
	$site_url = esc_url( admin_url( "/admin-ajax.php?post_id={$iframe_post_id}&action=jcd_form_builder&TB_iframe=true&width=768" ) );

	echo '<a href="' . $site_url . '&id=jcd_add_form" class="thickbox button" title="' . $title . '">'. $title .'</a>';
}


/**
 * Ajax action for showing form builder
 */
add_action( 'wp_ajax_jcd_form_builder', 'jcd_display_form_view' );
function jcd_display_form_view( ) {
	require_once get_template_directory() . '/functions/views/jcd-form-view.php';
	exit;
}

function jcd_formbuilder_esc_attr( $attr ) {
	$out = esc_attr( $attr );
	// we also have to entity-encode square brackets so they don't interfere with the shortcode parser
	// FIXME: do this better - just stripping out square brackets for now since they mysteriously keep reappearing
	$out = str_replace( '[', '', $out );
	$out = str_replace( ']', '', $out );
	return $out;
}

function jcd_formbuilder_sort_objects( $a, $b ) {
	if ( isset($a['order']) && isset($b['order']) )
		return $a['order'] - $b['order'];
	return 0;
}

// take an array of field types from the form builder, and construct a shortcode form
// returns both the shortcode form, and HTML markup representing a preview of the form
function jcd_formbuilder_ajax_shortcode() {
	check_ajax_referer( 'jcd_formbuilder_shortcode' );

	$attributes = array();

	foreach ( array( 'subject', 'to' ) as $attribute ) {
		if ( isset( $_POST[$attribute] ) && strlen( $_POST[$attribute] ) ) {
			$attributes[$attribute] = stripslashes( $_POST[$attribute] );
		}
	}

	if ( is_array( $_POST['fields'] ) ) {
		$fields = stripslashes_deep( $_POST['fields'] );
		usort( $fields, 'jcd_formbuilder_sort_objects' );

		$field_shortcodes = array();

		foreach ( $fields as $field ) {
			$field_attributes = array();

			if ( isset( $field['required'] ) && 'true' === $field['required'] ) {
				$field_attributes['required'] = 'true';
			}

			foreach ( array( 'options', 'label', 'type' ) as $attribute ) {
				if ( isset( $field[$attribute] ) ) {
					$field_attributes[$attribute] = $field[$attribute];
				}
			}

			$field_shortcodes[] = new Jcd_Contact_Form_Field( $field_attributes );
		}
	}

	$grunion = new Jcd_Contact_Form( $attributes, $field_shortcodes );

	die( "\n$grunion\n" );
}

// takes a post_id, extracts the contact-form shortcode from that post (if there is one), parses it,
// and constructs a json object representing its contents and attributes
function jcd_formbuilder_ajax_shortcode_to_json() {
	global $post, $grunion_form;
	
	check_ajax_referer( 'jcd_formbuilder_shortcode_to_json' );

	if ( !isset( $_POST['content'] ) || !is_numeric( $_POST['post_id'] ) ) {
		die( '-1' );
	}

	$content = stripslashes( $_POST['content'] );

	// doesn't look like a post with a [contact-form] already.
	if ( false === strpos( $content, '[jcd-contact-form' ) ) {
		die( '' );
	}

	$post = get_post( $_POST['post_id'] );

	do_shortcode( $content );

	$grunion = Jcd_Contact_Form::$last;

	$out = array(
		'to'      => '',
		'subject' => '',
		'fields'  => array(),
	);

	foreach ( $grunion->fields as $field ) {
		$out['fields'][$field->get_attribute( 'id' )] = $field->attributes;
	}

	$to = $grunion->get_attribute( 'to' );
	$subject = $grunion->get_attribute( 'subject' );
	foreach ( array( 'to', 'subject' ) as $attribute ) {
		$value = $grunion->get_attribute( $attribute );
		if ( isset( $grunion->defaults[$attribute] ) && $value == $grunion->defaults[$attribute] ) {
			$value = '';
		}
		$out[$attribute] = $value;
	}

	die( json_encode( $out ) );
}


add_action( 'wp_ajax_jcd_formbuilder_shortcode', 'jcd_formbuilder_ajax_shortcode' );
add_action( 'wp_ajax_jcd_formbuilder_shortcode_to_json', 'jcd_formbuilder_ajax_shortcode_to_json' );
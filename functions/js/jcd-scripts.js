/**
 *
 * Style Select
 *
 * Replace Select text
 * Dependencies: jQuery
 *
 */
(function ($) {
  styleSelect = {
    init: function () {
      $( '.select_wrapper').each(function () {
        $(this).prepend( '<span>' + $(this).find( '.jcd-input option:selected').text() + '</span>' );
      });
      $( 'select.jcd-input').live( 'change', function () {
        $(this).prev( 'span').replaceWith( '<span>' + $(this).find( 'option:selected').text() + '</span>' );
      });
      $( 'select.jcd-input').bind($.browser.msie ? 'click' : 'change', function(event) {
        $(this).prev( 'span').replaceWith( '<span>' + $(this).find( 'option:selected').text() + '</span>' );
      });
    }
  };
})(jQuery);


(function($){
$(document).ready(function() {
  styleSelect.init();

  $('.section-multiselect select').chosen()

  // On Select change
  .on('change', function(e, params){
    console.log(params);
    var $select = $(this),
        $wrapper = $select.closest('.section-multiselect'),
        $inputOrdered = $wrapper.find('.ordered');

    // Update the selectbox before get the ordered value
    // $select.trigger('chosen:updated');

    // Set ordered value
    setTimeout(function(){
      var value = $select.getSelectionOrder(),
          orderedValue = value.join(',');

      $inputOrdered.val( orderedValue );
    }, 100);
  });

  // On chosen read
  $('.section-multiselect select').on( 'chosen:ready', function(e){
    var $select = $(this),
        $wrapper = $select.closest('.section-multiselect'),
        $inputOrdered = $wrapper.find('.ordered');

    $select.setSelectionOrder( $inputOrdered.val().split(','), true );
  });

});
})(jQuery);

var jcdWidgets;
(function($) {

jcdWidgets = {

	init : function() {
		var rem, sidebars = $('div.widgets-sortables'), isRTL = !! ( 'undefined' != typeof isRtl && isRtl ),
			margin = ( isRtl ? 'marginRight' : 'marginLeft' ), the_id, 
                        widgetCounter = ($(".widgets-sortables").children("div.widget").length == '')? 0:$(".widgets-sortables").children("div.widget").length;

                widgetCounter++;
                
		sidebars.each(function(){
			if ( $(this).parent().hasClass('inactive') )
				return true;

			var h = 50, H = $(this).children('.widget').length;
			h = h + parseInt(H * 27, 10);
			$(this).css( 'minHeight', h + 'px' );
		});

		$(document.body).bind('click.widgets-toggle', function(e){
			var target = $(e.target), css = {}, widget, inside, w;

			if ( target.parents('.widget-top').length && ! target.parents('#available-widgets').length ) {
				widget = target.closest('div.widget');
				inside = widget.children('.widget-inside');
				w = parseInt( widget.find('input.widget-width').val(), 10 );

				if ( inside.is(':hidden') ) {
					if ( w > 250 && inside.closest('div.widgets-sortables').length ) {
						css['width'] = w + 30 + 'px';
						if ( inside.closest('div.widget-liquid-right').length )
							css[margin] = 235 - w + 'px';
						widget.css(css);
					}
					jcdWidgets.fixLabels(widget);
					inside.slideDown('fast');
				} else {
					inside.slideUp('fast', function() {
						widget.css({'width':'', margin:''});
					});
				}
				e.preventDefault();
			} else if ( target.hasClass('widget-control-remove') ) {
				jcdWidgets.remove( target.closest('div.widget'), 1, 1 );
				e.preventDefault();
			} else if ( target.hasClass('widget-control-close') ) {
				jcdWidgets.close( target.closest('div.widget') );
				e.preventDefault();
			}
		});

		sidebars.children('.widget').each(function() {
			jcdWidgets.appendTitle(this);
			if ( $('p.widget-error', this).length )
				$('a.widget-action', this).click();
		});

		$('#widget-list').children('.widget').draggable({
			connectToSortable: 'div.widgets-sortables',
			handle: '> .widget-top > .widget-title',
			distance: 2,
			helper: 'clone',
			zIndex: 100,
			containment: 'document',
			start: function(e,ui) {
				ui.helper.find('div.widget-description').hide();
				the_id = this.id;
			},
			stop: function(e,ui) {
				if ( rem )
					$(rem).hide();

				rem = '';
			}
		});

		sidebars.sortable({
			placeholder: 'widget-placeholder',
			items: '> .widget',
			handle: '> .widget-top > .widget-title',
			cursor: 'move',
			distance: 2,
			containment: 'document',
			start: function(e,ui) {
				ui.item.children('.widget-inside').hide();
				ui.item.css({margin:'', 'width':''});
			},
			stop: function(e,ui) {
				if ( ui.item.hasClass('ui-draggable') && ui.item.data('draggable') )
					ui.item.draggable('destroy');

				if ( ui.item.hasClass('deleting') ) {
					ui.item.remove();
					return;
				}

				var     n = widgetCounter,
					id = the_id,
					sb = $(this).attr('id');

				ui.item.css({margin:'', 'width':''});
				the_id = '';

                                ui.item.html( ui.item.html().replace(/<[^<>]+>/g, function(m){ return m.replace(/__i__|%i%/g, n); }) );
                                ui.item.attr( 'id', id.replace('__i__', n) );

                                widgetCounter++;
                                return;
			},
			receive: function(e, ui) {
				var sender = $(ui.sender);

				if ( !$(this).is(':visible') || this.id.indexOf('orphaned_widgets') != -1 )
					sender.sortable('cancel');

				if ( sender.attr('id').indexOf('orphaned_widgets') != -1 && !sender.children('.widget').length ) {
					sender.parents('.orphan-sidebar').slideUp(400, function(){ $(this).remove(); });
				}
			}
		}).sortable('option', 'connectWith', 'div.widgets-sortables').parent().filter('.closed').children('.widgets-sortables').sortable('disable');

		$('#available-widgets').droppable({
			tolerance: 'pointer',
			accept: function(o){
				return $(o).parent().attr('id') != 'widget-list';
			},
			drop: function(e,ui) {
				ui.draggable.addClass('deleting');
				$('#removing-widget').hide().children('span').html('');
			},
			over: function(e,ui) {
				ui.draggable.addClass('deleting');
				$('div.widget-placeholder').hide();

				if ( ui.draggable.hasClass('ui-sortable-helper') )
					$('#removing-widget').show().children('span')
					.html( ui.draggable.find('div.widget-title').children('h4').html() );
			},
			out: function(e,ui) {
				ui.draggable.removeClass('deleting');
				$('div.widget-placeholder').show();
				$('#removing-widget').hide().children('span').html('');
			}
		});
	},

	remove : function(widget, del, animate) {
		var sb = widget.closest('div.widgets-sortables').attr('id'), data = widget.find('form').serialize(), a;
		widget = $(widget);
		$('.spinner', widget).show();

                if ( del ) {
                        if ( !$('input.widget_number', widget).val() ) {
                                id = $('input.widget-id', widget).val();
                                $('#available-widgets').find('input.widget-id').each(function(){
                                        if ( $(this).val() == id )
                                                $(this).closest('div.widget').show();
                                });
                        }

                        if ( animate ) {
                                widget.slideUp('fast', function(){
                                        $(this).remove();
                                });
                        } else {
                                widget.remove();
                                jcdWidgets.resize();
                        }
                } else {
                        $('.spinner').hide();
                        if ( r && r.length > 2 ) {
                                $('div.widget-content', widget).html(r);
                                jcdWidgets.appendTitle(widget);
                                jcdWidgets.fixLabels(widget);
                        }
                }
	},

	appendTitle : function(widget) {
		var title = $('input[id*="-title"]', widget).val() || '';

		if ( title )
			title = ': ' + title.replace(/<[^<>]+>/g, '').replace(/</g, '&lt;').replace(/>/g, '&gt;');

		$(widget).children('.widget-top').children('.widget-title').children()
				.children('.in-widget-title').html(title);

	},

	resize : function() {
		$('div.widgets-sortables').each(function(){
			if ( $(this).parent().hasClass('inactive') )
				return true;

			var h = 50, H = $(this).children('.widget').length;
			h = h + parseInt(H * 48, 10);
			$(this).css( 'minHeight', h + 'px' );
		});
	},

	fixLabels : function(widget) {
		widget.children('.widget-inside').find('label').each(function(){
			var f = $(this).attr('for');
			if ( f && f == $('input', this).attr('id') )
				$(this).removeAttr('for');
		});
	},

	close : function(widget) {
		widget.children('.widget-inside').slideUp('fast', function(){
			widget.css({'width':'', margin:''});
		});
	}
};

$(document).ready(function($){ jcdWidgets.init(); });

})(jQuery);

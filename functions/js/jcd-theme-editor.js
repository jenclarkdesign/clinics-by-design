(function(global, $){
$(document).ready(function(){

	var editor = CodeMirror.fromTextArea( document.getElementById('newcontent'), {
		mode: jcd_editor_mime,
		styleActiveLine: true,
		lineNumbers: true,
		lineWrapping: false
	} );

});
})(this, jQuery);
<?php
// File Security Check
if ( ! defined( 'ABSPATH' ) ) exit;

/*-----------------------------------------------------------------------------------*/
/* Jcd Framework Version & Theme Version */
/*-----------------------------------------------------------------------------------*/

function jcd_version_init() {
    $jcd_framework_version = '1.0.0';
    if ( get_option( 'jcd_framework_version' ) != $jcd_framework_version ) {
    	update_option( 'jcd_framework_version', $jcd_framework_version );
    }
}
add_action( 'init', 'jcd_version_init', 10 );

function jcd_version () {
    $data = jcdframework_get_theme_version_data();
	echo "\n<!-- Theme version -->\n";
    if ( isset( $data['is_child'] ) && true == $data['is_child'] ) echo '<meta name="generator" content="'. esc_attr( $data['child_theme_name'] . ' ' . $data['child_theme_version'] ) . '" />' ."\n";
    echo '<meta name="generator" content="'. esc_attr( $data['theme_name'] . ' ' . $data['theme_version'] ) . '" />' ."\n";
    echo '<meta name="generator" content="JcdFramework '. esc_attr( $data['framework_version'] ) .'" />' ."\n";
} // End jcd_version()

add_action( 'wp_head', 'jcd_version', 10 );

/*-----------------------------------------------------------------------------------*/
/* Load the required Framework Files */
/*-----------------------------------------------------------------------------------*/

$functions_path = get_template_directory() . '/functions/';

require_once ( $functions_path . 'admin-functions.php' );				// Custom functions and plugins
require_once ( $functions_path . 'admin-setup.php' );					// Options panel variables and functions
require_once ( $functions_path . 'admin-custom.php' );					// Custom fields
require_once ( $functions_path . 'admin-interface.php' );				// Admin Interfaces (options,framework, seo)
// require_once ( $functions_path . 'admin-sbm.php' ); 					// Framework Sidebar Manager
require_once ( $functions_path . 'admin-medialibrary-uploader.php' );                   // Framework Media Library Uploader Functions
require_once ( $functions_path . 'admin-hooks.php' );					// Definition of JcdHooks
// require_once ( $functions_path . 'admin-theme-editor.php' );            // Add syntax highlighting for theme editor
// require_once ( $functions_path . 'admin-plugin-activation.php' );      	// Plugin required for activation

if( current_theme_supports('jcd_mega_menu') ) require_once ( $functions_path . 'admin-megamenu.php' ); 				// Megamenu
if( current_theme_supports('jcd_homepage_builder') ) require_once ( $functions_path . 'admin-page-template.php' );    // homepage builder
if( current_theme_supports('jcd_form_builder') ) require_once ( $functions_path . 'admin-formbuilder.php' );          // form builder
if( current_theme_supports('jcd_coming_soon') ) require_once ( $functions_path . 'admin-comingsoon.php' );            // coming soon
if( current_theme_supports('jcd_maintenance_mode') ) require_once ( $functions_path . 'admin-maintenance-mode.php' ); // maintenance mode
if( current_theme_supports('jcd_advance_custom_fields') ) require_once ( $functions_path . 'admin-advance-custom-fields.php' ); // maintenance mode
// require_once( $functions_path . 'admin-theme-upgrader.php' ); // Theme Upgrader
if(is_admin()) require_once ( $functions_path . 'admin-backup.php' ); // Theme Options Backup

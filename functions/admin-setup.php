<?php

/*-------------------------------------------------------------------------------------

TABLE OF CONTENTS

- Redirect to "Theme Options" screen (hooked onto jcd_theme_activate at 10).
- Flush rewrite rules to refresh permalinks for custom post types, etc.
- Show Options Panel after activate
- Admin Backend
	- Setup Custom Navigation
- Output HEAD - jcd_wp_head()
	- Output alternative stylesheet
	- Output custom favicon
	- Load textdomains
- Post Images from WP2.9+ integration
- Enqueue comment reply script

-------------------------------------------------------------------------------------*/

define( 'THEME_FRAMEWORK', 'jcd' );

/*-----------------------------------------------------------------------------------*/
/* Redirect to "Theme Options" screen (hooked onto jcd_theme_activate at 10). */
/*-----------------------------------------------------------------------------------*/
add_action( 'jcd_theme_activate', 'jcd_themeoptions_redirect', 10 );

function jcd_themeoptions_redirect () {
	// Do redirect
	header( 'Location: ' . admin_url() . 'admin.php?page=jcd' );
} // End jcd_themeoptions_redirect()

/*-----------------------------------------------------------------------------------*/
/* Flush rewrite rules to refresh permalinks for custom post types, etc. */
/*-----------------------------------------------------------------------------------*/

function jcd_flush_rewriterules () {
	flush_rewrite_rules();
} // End jcd_flush_rewriterules()

/*-----------------------------------------------------------------------------------*/
/* Add default options and show Options Panel after activate  */
/*-----------------------------------------------------------------------------------*/
global $pagenow;

if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) {
	// Call action that sets.
	add_action( 'admin_head','jcd_option_setup' );

	// Flush rewrite rules.
	add_action( 'admin_head', 'jcd_flush_rewriterules', 9 );

	// Custom action for theme-setup (redirect is at priority 10).
	do_action( 'jcd_theme_activate' );
}


if ( ! function_exists( 'jcd_option_setup' ) ) {
	function jcd_option_setup(){

		//Update EMPTY options
		$jcd_array = array();
		add_option( 'jcd_options', $jcd_array );

		$template = get_option( 'jcd_template' );
		$saved_options = get_option( 'jcd_options' );

		foreach ( (array) $template as $option ) {
			if ($option['type'] != 'heading'){
				$id = $option['id'];
				$std = isset( $option['std'] ) ? $option['std'] : NULL;
				$db_option = get_option($id);
				if (empty($db_option)){
					if (is_array($option['type'])) {
						foreach ($option['type'] as $child){
							$c_id = $child['id'];
							$c_std = $child['std'];
							$db_option = get_option($c_id);
							if (!empty($db_option)){
								update_option($c_id,$db_option);
								$jcd_array[$id] = $db_option;
							} else {
								$jcd_array[$c_id] = $c_std;
							}
						}
					} else {
						update_option($id,$std);
						$jcd_array[$id] = $std;
					}
				} else { //So just store the old values over again.
					$jcd_array[$id] = $db_option;
				}
			}
		}

		// Allow child themes/plugins to filter here.
		$jcd_array = apply_filters( 'jcd_options_array', $jcd_array );

		update_option( 'jcd_options', $jcd_array );
	}
}


/*-----------------------------------------------------------------------------------*/
/* Output HEAD - jcd_wp_head */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'jcd_wp_head' ) ) {
	function jcd_wp_head() {

		do_action( 'jcd_wp_head_before' );

		// Output alternative stylesheet
		// if ( function_exists( 'jcd_output_alt_stylesheet' ) )
		// 	jcd_output_alt_stylesheet();

		// Output custom favicon
		// if ( function_exists( 'jcd_output_custom_favicon' ) )
		// 	jcd_output_custom_favicon();

		// Output CSS from standarized styling options
		if ( function_exists( 'jcd_head_css' ) )
			jcd_head_css();

		do_action( 'jcd_wp_head_after' );
	} // End jcd_wp_head()
}
add_action( 'wp_head', 'jcd_wp_head', 10 );

/*-----------------------------------------------------------------------------------*/
/* Output alternative stylesheet - jcd_output_alt_stylesheet */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'jcd_output_alt_stylesheet' ) ) {
	function jcd_output_alt_stylesheet() {

		$style = '';

		if ( isset( $_REQUEST['style'] ) ) {
			// Sanitize requested value.
			$requested_style = strtolower( strip_tags( trim( $_REQUEST['style'] ) ) );
			$style = $requested_style;
		}

		echo "<!-- Alt Stylesheet -->\n";
		if ($style != '') {
			echo '<link id="jcd-alt-stylesheet" href="'. get_template_directory_uri() . '/skins/'. $style . '.css" rel="stylesheet" type="text/css" />' . "\n\n";
		} else {
			$style = get_option( 'jcd_alt_stylesheet' );
			if( $style != '' ) {
				// Sanitize value.
				$style = strtolower( strip_tags( trim( $style ) ) );
				echo '<link id="jcd-alt-stylesheet" href="'. get_template_directory_uri() . '/skins/'. $style .'" rel="stylesheet" type="text/css" />' . "\n\n";
			} else {
				echo '<link id="jcd-alt-stylesheet" href="'. get_template_directory_uri() . '/skins/default.css" rel="stylesheet" type="text/css" />' . "\n\n";
			}
		}

	} // End jcd_output_alt_stylesheet()
}

/*-----------------------------------------------------------------------------------*/
/* Output favicon link - jcd_custom_favicon() */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'jcd_output_custom_favicon' ) ) {
	function jcd_output_custom_favicon() {
		// Favicon
		$favicon = '';
		$favicon = get_option( 'jcd_custom_favicon' );

		// Allow child themes/plugins to filter here.
		$favicon = apply_filters( 'jcd_custom_favicon', $favicon );
		if( $favicon != '' )
                    $link_fav   = esc_url( $favicon );
                else
                    $link_fav   = get_template_directory_uri () . '/images/favicon.ico';

                echo "<!-- Custom Favicon -->\n";
	        echo '<link rel="shortcut icon" href="' .  $link_fav  . '"/>' . "\n";
                echo '<!-- Le fav and touch icons -->' . "\n";
	        echo '<link rel="apple-touch-icon" href="' .  get_template_directory_uri()  . '/images/apple-touch-icon.png"/>' . "\n";
	        echo '<link rel="apple-touch-icon" sizes="72x72" href="' .  get_template_directory_uri()  . '/images/apple-touch-icon-72x72-precomposed.png"/>' . "\n";
	        echo '<link rel="apple-touch-icon" sizes="114x114" href="' .  get_template_directory_uri()  . '/images/apple-touch-icon-114x114-precomposed.png"/>' . "\n\n";

	} // End jcd_output_custom_favicon()
}

/*-----------------------------------------------------------------------------------*/
/* Load textdomain - jcd_load_textdomain() */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'jcd_load_textdomain' ) ) {
	function jcd_load_textdomain() {

		load_theme_textdomain( 'jcd' );
		load_theme_textdomain( 'jcd', get_template_directory() . '/language' );
		if ( function_exists( 'load_child_theme_textdomain' ) )
			load_child_theme_textdomain( 'jcd' );

	}
}

add_action( 'init', 'jcd_load_textdomain', 10 );

/*-----------------------------------------------------------------------------------*/
/* Output CSS from standarized options */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'jcd_head_css' ) ) {
	function jcd_head_css() {

		$output = '';
                $custom_css = get_option( 'jcd_custom_css' );

		if ($custom_css <> '')
			$output .= $custom_css . "\n";


		// Output styles
		if ($output <> '') {
			$output = strip_tags($output);
			echo "<!-- Custom CSS -->\n";
			$output = "<style type=\"text/css\">\n" . $output . "</style>\n\n";
			echo $output;
		}

	}
}


/*-----------------------------------------------------------------------------------*/
/* Post Images from WP2.9+ integration /*
/*-----------------------------------------------------------------------------------*/
if( function_exists( 'add_theme_support' ) ) {
	if( get_option( 'jcd_post_image_support' ) == 'true' ) {
		add_theme_support( 'post-thumbnails' );
		// set height, width and crop if dynamic resize functionality isn't enabled
		if ( get_option( 'jcd_pis_resize' ) != 'true' ) {
			$thumb_width = get_option( 'jcd_thumb_w' );
			$thumb_height = get_option( 'jcd_thumb_h' );
			$single_width = get_option( 'jcd_single_w' );
			$single_height = get_option( 'jcd_single_h' );
			$hard_crop = get_option( 'jcd_pis_hard_crop' );
			if($hard_crop == 'true') { $hard_crop = true; } else { $hard_crop = false; }
			set_post_thumbnail_size( $thumb_width, $thumb_height, $hard_crop ); // Normal post thumbnails
			add_image_size( 'single-post-thumbnail', $single_width, $single_height, $hard_crop );
		}
	}
}


/*-----------------------------------------------------------------------------------*/
/* Enqueue comment reply script */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'jcd_comment_reply' ) ) {
	function jcd_comment_reply() {
		if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
	} // End jcd_comment_reply()
}
add_action( 'get_header', 'jcd_comment_reply', 10 );
?>

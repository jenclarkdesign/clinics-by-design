<?php
/**
 * This file holds various functions to create a maintenance mode
 *
 * @author	Arif Widi @mambows
 * @copyright	Copyright ( c ) Arif Widi
 * @link	http://jcd.me
 * @since	Version 1.2
 * @package 	JcdFramework
 */

/* 
 * Hook into Jcd options array 
 */
function jcd_maintenance_options( $options ) {

	$options[] = array( "name" => "Maintenance Mode",
						"type" => "heading",
			          	"icon" => "layout" );

	$options[] = array( "name" => "Enable Maintenance Mode",
						"desc" => "Check this if you want to enable maintenance mode.",
						"id" => "jcd_maintenance_enable",
						"type" => "checkbox" );

	$options[] = array( "name" => "Additional Text",
						"desc" => "Additional text that will be shown.",
						"id" => "jcd_maintenance_text",
						"type" => "textarea" );

	$options[] = array( "name" => "Background Image", 
						"desc" => "Change the background image of the Maintenance page.",
						"id" => "jcd_maintenance_background", 
						"type" => "upload" );

	return $options;

}
add_filter( 'jcd_options_data', 'jcd_maintenance_options' );

/**
 * Show coming soon page for unauthorized users
 */
function jcd_show_maintenance() {

	$is_enable = get_option( 'jcd_maintenance_enable' );	

	// If maintenance mode is active
	if( "true" == $is_enable ) {
		if( !is_user_logged_in()
			&& ! strstr($_SERVER['PHP_SELF'], 'feed/')
			&& ! strstr($_SERVER['PHP_SELF'], 'wp-admin/')
			&& ! strstr($_SERVER['PHP_SELF'], 'wp-login.php')
			&& ! strstr($_SERVER['PHP_SELF'], 'async-upload.php')
			&& ! strstr($_SERVER['PHP_SELF'], 'upgrade.php')
			&& ! strstr($_SERVER['PHP_SELF'], 'trackback/')
			&& ! strstr($_SERVER['PHP_SELF'], '/xmlrpc.php') ) {

			global $is_maintenance;
			$is_maintenance = true;

			// It will find for template file inside themes, if not found, 
			// the default template will be loaded
			locate_template( array( 'template-maintenance.php', 'functions/views/jcd-maintenance-view.php' ), true, true );
			die;
		}
	}

}
add_action( 'init', 'jcd_show_maintenance' );

/**
 * Enqueue scripts and styles
 */
function jcd_maintenance_scripts() {
	global $is_maintenance;

	if( $is_maintenance ) {
		wp_enqueue_style( 'maintenance-page', get_template_directory_uri() . '/functions/css/jcd-maintenance.css' );

		if( get_option( 'jcd_maintenance_background' ) != '' ) {
			wp_enqueue_script( 'jquery-anystretch', get_template_directory_uri() . '/functions/js/jquery.anystretch.js', array('jquery') );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'jcd_maintenance_scripts' );

/**
 * Add notification on the admin bar, when maintenance mode is active
 */
function jcd_maintenance_adminbar( $admin_bar ) {
	if( 'true' == get_option( 'jcd_maintenance_enable' ) ) {
		$admin_bar->add_menu( array(
			'parent' => 'top-secondary',
			'id' => 'jcd_comingsoon_active',
			'href' => admin_url( 'admin.php?page=jcd#jcd-option-maintenancemode' ),
			'title' => __('Maintenance Mode Active', 'jcd')
		) );
	}
}
add_action( 'admin_bar_menu', 'jcd_maintenance_adminbar', 100 );
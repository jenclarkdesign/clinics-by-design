<?php

/**
 * Check and upgrade Theme from Themeforest automatically
 */
class Jcd_Theme_Upgrader {
  /**
   * The Envato Protected API object
   */
  protected $protected_api;
  protected $theme_upgrader;

  /**
   * Constructor Method
   */
  public function __construct() {
    $this->_constant();
    $this->_includes();
    $this->_hooks();
  }

  /**
   * Define Constant
   */
  protected function _constant() {
    if( !defined( 'NESIA_FRAMEWORK_DIR' ) )
      define( 'NESIA_FRAMEWORK_DIR', get_template_directory() . '/functions/' );

    if( !defined( 'NESIA_FRAMEWORK_URL' ) )
      define( 'NESIA_FRAMEWORK_URL', get_template_directory_uri() . '/functions/' );
  }

  /**
   * Include required files
   */
  protected function _includes() {
    /* load required files */
    foreach ( array( 'class-envato-wordpress-theme-upgrader', 'class-envato-backup' ) as $file )
      require_once( NESIA_FRAMEWORK_DIR . 'envato-wordpress-toolkit-library/' . $file . '.php' );
  }

  /**
   * Setup the default filters and actions
   */
  protected function _hooks() {
    add_action( 'admin_menu', array( &$this, '_add_admin_submenu' ) );
    add_action( 'wp_ajax_save_update_settings', array( &$this, 'ajax_save_settings' ) );

    // Add admin notices when there is new update
    add_action( 'admin_notices', array( &$this, 'new_update_notices' ) );

    /**
     * change link URL & text after install 
     */
    add_filter( 'update_theme_complete_actions', array( &$this, '_complete_actions' ), 10, 1 );
  }

  /**
   * Add Admin Submenu
   */
  public function _add_admin_submenu() {
    $submenu_page = add_submenu_page( 'jcd', __('Theme Update', 'jcd'), __('Update Theme', 'jcd'), 'manage_options', 'jcd_theme_updater', array( &$this, '_theme_updater_page' ) );

    add_action('admin_print_scripts-' . $submenu_page, array( &$this, '_updater_load_scripts' ) );
    add_action('admin_print_styles-' . $submenu_page, array( &$this, '_updater_load_styles' ) );
  }

  /**
   * Load Scripts
   */
  public function _updater_load_scripts() {
    wp_enqueue_script( 'thickbox' );
    wp_enqueue_script( 'jcd-scripts', get_template_directory_uri() . '/functions/js/jcd-scripts.js', array( 'jquery' ) );
    wp_enqueue_script( 'jcd-admin-interface', get_template_directory_uri() . '/functions/js/jcd-admin-interface.js', array( 'jquery' ), '5.0.0' );
    wp_enqueue_script( 'jgrowl', get_template_directory_uri() . '/functions/js/jquery.jgrowl_min.js', array( 'jquery' ) );
  }

  /**
   * Load Styles
   */
  public function _updater_load_styles() {
    wp_enqueue_style( 'jcd-admin-interface', get_template_directory_uri() . '/functions/admin-style.css', '', '5.0.0' );
    wp_enqueue_style( 'jgrowl', get_template_directory_uri() . '/functions/css/jquery.jgrowl.css' );
  }

  /**
   * Add Notification when there is new update available
   */
  public function new_update_notices() {
    $themename =  get_option( 'jcd_themename' );
    $shortname =  get_option( 'jcd_shortname' );

    $jcd_envato_username = get_option( 'jcd_envato_username' );
    $jcd_envato_api = get_option( 'jcd_envato_api' );

    $upgrader = new Envato_WordPress_Theme_Upgrader( $jcd_envato_username, $jcd_envato_api );
    $update_url = admin_url('admin.php?page=jcd_theme_updater#jcd-option-update-theme');
    
    if( $upgrader->check_for_theme_update( $themename )->updated_themes_count ) { ?>
      <div class="updated">
        <p><?php _e( sprintf('There is a new version of %s available. <a href="%s">Please update now.</a>', $themename, $update_url), 'jcd' ); ?></p>
      </div>
    <?php }
  }

  /**
   * Show Theme Updater Page
   */
  public function _theme_updater_page() {
    $themename =  get_option( 'jcd_themename' );
    $shortname =  get_option( 'jcd_shortname' );
    $manualurl =  get_option( 'jcd_manual' );
    $supporturl = "http://support.jcd.me/";

    $theme_data = jcdframework_get_theme_version_data();
    $local_version = $theme_data['theme_version'];

    $jcd_framework_version = get_option( 'jcd_framework_version' );
    $jcd_envato_username = get_option( 'jcd_envato_username' );
    $jcd_envato_api = get_option( 'jcd_envato_api' );

    $protected_api = new Envato_Protected_API( $jcd_envato_username, $jcd_envato_api);

    /* display API errors */
    if ( $errors = $protected_api->api_errors() ) {
      foreach( $errors as $k => $v ) {
        if ( $k !== 'http_code' && ( $user_name || $api_key ) )
          echo '<div class="error"><p>' . $v . '</p></div>';
      }
    }

    /* execute theme actions */
    if ( isset( $_GET['action'] ) && isset( $_GET['theme'] ) ) {
      if ( 'upgrade-theme' == $_GET['action'] && isset( $_GET['item_id'] ) ) {
        $this->_upgrade_theme( $_GET['theme'], $_GET['item_id'] );
      }

    /* display normal views */
    } else {

      /* get current theme */
      $theme_data = get_theme_data( get_theme_root() . '/' . $themename . '/style.css' );
      $get_theme = wp_get_theme( $themename );

      /** Get Purchased Theme **/
      $themes = $protected_api->wp_list_themes();
      $theme_id = '4571164';
      $item_detail_html = '';

      if( $themes ) {

        foreach( $themes as $theme ) {
          // Only get current theme
          if( $theme->theme_name !== $themename ) continue;

          /* setup theme datas */
          $content = '';
          $links = array();
          $latest_version = $theme->version;
          $item_id = $theme->item_id;
          $template = $get_theme['Template'];
          $stylesheet = $get_theme['Stylesheet'];
          $title = $get_theme['Title'];
          $version = $get_theme['Version'];
          $description = $get_theme['Description'];
          $author = $get_theme['Author'];
          $parent_theme = '';
          $tags = '';

          $screenshot = $get_theme['Screenshot'];
          $stylesheet_dir = $get_theme['Stylesheet Dir'];
          $template_dir = $get_theme['Template Dir'];
          $parent_theme = $get_theme['Parent Theme'];
          $theme_root = $get_theme['Theme Root'];
          $theme_root_uri = $get_theme['Theme Root URI'];

          /* setup the item details */
          $item_details = $protected_api->item_details( $item_id );

          $has_update = ( version_compare( $version, $latest_version, '<' ) ) ? TRUE : FALSE;
          $details_url = htmlspecialchars( add_query_arg( array( 'TB_iframe' => 'true', 'width' => 1024, 'height' => 800 ), $item_details->url ) );
          $update_url = wp_nonce_url( admin_url( 'admin.php?page=jcd_theme_updater&action=upgrade-theme&amp;theme=' . $stylesheet . '&amp;item_id=' . $item_id ), 'upgrade-theme_' . $stylesheet );

          /* Theme Title message */
          $content.= '<h3>' . $title . ' ' . $version . ' by ' . $author . '</h3>';

          /* Theme Description */
          if ( $description ) {
            $content.= '<p class="description">' . $description . '</p>';
          }

          /* Links list */
          $links[] = '<a href="' . $details_url . '" class="thickbox thickbox-preview" title="' . esc_attr( sprintf( __( 'View version %1$s details', 'jcd' ), $latest_version ) ) . '">' . esc_attr( sprintf( __( 'View version %1$s details', 'jcd' ), $latest_version ) ) . '</a>';
          $content.= '<div class="update-info">' . implode( ' | ', $links ) . '</div>';

          /* Theme path information */
          if ( current_user_can( 'edit_themes' ) ) {
            $content.= '<p>' . sprintf( __( 'All of this theme&#8217;s files are located in <code>%2$s</code>.', 'jcd' ), $title, str_replace( WP_CONTENT_DIR, '', $template_dir ), str_replace( WP_CONTENT_DIR, '', $stylesheet_dir ) ) . '</p>';
          }

          /* Upgrade/Install message */
          if ( $has_update ) {
            if ( ! current_user_can( 'update_themes' ) ) {
              $content.= sprintf( '<div class="updated below-h2"><p><strong>' . __( 'There is a new version of %1$s available. <a href="%2$s" class="thickbox thickbox-preview" title="%1$s">View version %3$s details</a>.', 'jcd' ) . '</strong></p></div>', $title, $details_url, $latest_version );
            } else {
              $content.= sprintf( '<div class="updated below-h2"><p><strong>' . __( 'There is a new version of %1$s available. <a href="%2$s" class="thickbox thickbox-preview" title="%1$s">View version %3$s details</a> or <a href="%4$s">update automatically</a>.', 'jcd' ) . '</strong></p></div>', $title, $details_url, $latest_version, $update_url );
            }
          }

          /* put the HTML into a variable */
          $item_detail_html .= '
          <div class="item-detail-wrapper">
            <div class="item-thumbnail">
              <img src="' . $item_details->thumbnail  . '" alt="' . $title . '" />
            </div>
            <div class="item-detail">
              '. $content .'
            </div>
          </div>
          ';
        }

      }

      include('views/jcd-theme-updater-view.php');

    } // endif
  }

  /**
   * Upgrade Theme
   */
  public function _upgrade_theme( $theme, $item_id ) {
    global $current_screen;

    check_admin_referer( 'upgrade-theme_' . $theme );

    if ( ! current_user_can( 'update_themes' ) )
      wp_die( __( 'You do not have sufficient permissions to update themes for this site.', 'jcd' ) );

    $title = __( 'Update Theme', 'jcd' );
    $nonce = 'upgrade-theme_' . $theme;
    $url = admin_url( 'admin.php?page=jcd_theme_updater&action=upgrade-theme&theme=' . $theme . '&item_id=' . $item_id );

    /* trick WP into thinking it's the themes page for the icon32 */
    $current_screen->parent_base = 'themes';

    /* new Envato_Theme_Upgrader */
    $jcd_envato_username = get_option( 'jcd_envato_username' );
    $jcd_envato_api = get_option( 'jcd_envato_api' );
    $upgrader = new Envato_WordPress_Theme_Upgrader( $jcd_envato_username, $jcd_envato_api, $nonce, $url );
    
    $upgrader->upgrade_theme( $theme );
  }

  /**
   * Change the text on the install or upgrade screen
   */
  public function _complete_actions( $actions ) {
    if ( isset( $_GET['page'] ) && isset( $_GET['action'] ) ) {
      $page   = $_GET['page'];
      $action = $_GET['action'];
      unset($actions['preview']);
      unset($actions['activate']);
      if ( $page == 'jcd_theme_updater' ) {
        if ( 'upgrade-theme' == $action ) {
          $actions['themes_page'] = '<a href="' . admin_url( 'admin.php?page=jcd_theme_updater' ) . '" title="' . esc_attr__( sprintf( __( 'Return to %s', 'envato' ), 'Jcd Theme Panel' ) ) . '" target="_parent">' . sprintf( __( 'Return to %s', 'envato' ), 'Jcd Theme Panel' ) . '</a>';
        }
      }
    }
    return $actions;
  }

  /**
   * Validate Envato API
   */
  public function validate_api( $user_name, $api_key ) {
    $url = "http://marketplace.envato.com/api/edge/$user_name/$api_key/account.json";
    $data = wp_remote_get( $url );
    if( !is_wp_error($data) ) {
      if( $data['response']['code'] == 200 ) {
        return true;
      }
    }
  }

  /**
   * Ajax Callback for handling form submission
   */
  public function ajax_save_settings() {
    $messages = array();
    $status = 'failure';

    if( isset( $_POST ) ) {
      if( !check_ajax_referer('jcdframework-theme-envato-settings') ) {
        $messages[] = __('Security code not matched');
        return;
      }

      $user_name = $_POST['jcd_envato_username'];
      $api_key = $_POST['jcd_envato_api'];

      if( !$user_name ) {
        $messages[] = __('Please input your Envato Username', 'jcd');
      }

      if( !$api_key ) {
        $messages[] = __('Please input your Envato API Key', 'jcd');
      }

      if( $user_name && $api_key ) {
        if( $tes = $this->validate_api( $user_name, $api_key ) ) {
          update_option( 'jcd_envato_username', $user_name );
          update_option( 'jcd_envato_api', $api_key ); 
          $messages[] = __('Settings Saved', 'jcd');
          $status = 'success';
        } else {
          $messages[] = __('Your API Key is not valid, please double check your API Key', 'jcd');
        }
      }

    } else {
      $messages[] = __('No data sent', 'jcd');
    }

    header('Content-Type: application/json');
    echo json_encode( array(
      'messages' => $messages,
      'status' => $status
    ) );
    exit;
  }

}

$jcd_theme_upgrader = new Jcd_Theme_Upgrader();
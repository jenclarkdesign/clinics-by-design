<?php
/**
 * Template for Jcd Form Builder
 *
 * @author Arif Widipratomo
 * @link  http://jcd.me
 * @since Version 1.1.2
 * @package  JcdFramework
 */

$framework_version = get_option( 'jcd_framework_version' );
$framework_url = get_template_directory_uri() . '/functions/';

wp_register_script( 'jcd-form-builder', $framework_url . 'js/jcd-form-builder.js', array( 'jquery-ui-sortable', 'jquery-ui-draggable' ), $framework_version );
wp_localize_script( 'jcd-form-builder', 'JcdFB_i18n', array(
	'nameLabel' => esc_attr( _x( 'Name', 'Label for HTML form "Name" field in contact form builder', 'jcd' ) ),
	'emailLabel' => esc_attr( _x( 'Email', 'Label for HTML form "Email" field in contact form builder', 'jcd' ) ),
	'urlLabel' => esc_attr( _x( 'Website', 'Label for HTML form "URL/Website" field in contact form builder', 'jcd' ) ),
	'subjectLabel' => esc_attr( _x( 'Subject', 'Label for HTML form "Subject" field in contact form builder', 'jcd' ) ),
	'commentLabel' => esc_attr( _x( 'Comment', 'Label for HTML form "Comment/Response" field in contact form builder', 'jcd' ) ),
	'newLabel' => esc_attr( _x( 'New Field', 'Default label for new HTML form field in contact form builder', 'jcd' ) ),
	'optionsLabel' => esc_attr( _x( 'Options', 'Label for the set of options to be included in a user-created dropdown in contact form builder', 'jcd' ) ),
	'optionsLabel' => esc_attr( _x( 'Option', 'Label for an option to be included in a user-created dropdown in contact form builder', 'jcd' ) ),
	'firstOptionLabel' => esc_attr( _x( 'First option', 'Default label for the first option to be included in a user-created dropdown in contact form builder', 'jcd' ) ),
	'problemGeneratingForm' => esc_attr( _x( "Oops, there was a problem generating your form.  You'll likely need to try again.", 'error message in contact form builder', 'jcd' ) ),
	'moveInstructions' => esc_attr__( "Drag up or down\nto re-arrange", 'jcd' ),
	'moveLabel' => esc_attr( _x( 'move', 'Label to drag HTML form fields around to change their order in contact form builder', 'jcd' ) ),
	'editLabel' => esc_attr( _x( 'edit', 'Link to edit an HTML form field in contact form builder', 'jcd' ) ),
	'savedMessage' => esc_attr__( 'Saved successfully', 'jcd' ),
	'requiredLabel' => esc_attr( _x( '(required)', 'This HTML form field is marked as required by the user in contact form builder', 'jcd' ) ),
	'exitConfirmMessage' => esc_attr__( 'Are you sure you want to exit the form editor without saving?  Any changes you have made will be lost.', 'jcd' ),
) );

wp_register_style( 'jcd-form-builder-admin', $framework_url . 'css/jcd-form-builder-admin.css', array(), $framework_version );

?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php esc_html_e( 'Contact Form', 'jcd' ); ?></title>

		<?php wp_print_styles( 'jcd-form-builder-admin' ); ?>

		<script type="text/javascript">
			var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
			var postId = <?php echo absint( $_GET['post_id'] ); ?>;
			var ajax_nonce_shortcode = '<?php echo wp_create_nonce( 'jcd_formbuilder_shortcode' ); ?>';
			var ajax_nonce_json = '<?php echo wp_create_nonce( 'jcd_formbuilder_shortcode_to_json' ); ?>';
		</script>
		<?php wp_print_scripts( 'jcd-form-builder' ); ?>

		<script type="text/javascript">
			jQuery(document).ready(function () {
				JcdFB.ContactForm.init();
				JcdFB.ContactForm.resizePop();
			});
			jQuery(window).resize(function() {
			  	setTimeout(function () { JcdFB.ContactForm.resizePop(); }, 50);
			});
		</script>
	</head>
	
	<body>
		
		<div class="jcd-fb-header">
			<div id="jcd-fb-success" class="jcd-fb-success" style="display: none;"><?php esc_html_e( 'Your new field was saved successfully', 'jcd' ); ?></div>
			<ul id="sidemenu">
				<li id="tab-preview"><a class="current" href=""><?php esc_html_e( 'Form builder', 'jcd' ); ?></a></li>
				<li id="tab-settings"><a href=""><?php esc_html_e( 'Email notifications', 'jcd' ); ?></a></li>
			</ul>
		</div>
		<!-- .jcd-fb-header -->
	
		<div class="jcd-fb-right">
			<div id="jcd-fb-desc" class="jcd-fb-desc">
				<h3><?php esc_html_e( 'How does this work?', 'jcd' ); ?></h3>
				<p><?php esc_html_e( 'By adding a contact form, your readers will be able to submit feedback to you. All feedback is automatically scanned for spam, and the legitimate feedback will be emailed to you.', 'jcd' ); ?></p>
				<h3 style="margin-top: 21px;"><?php esc_html_e( 'Can I add more fields?', 'jcd' ); ?></h3>
				<p><?php printf(
					esc_html( _x( 'Sure thing. %1$s to add a new text box, textarea, radio, checkbox, or dropdown field.', '%1$s = "Click here" in an HTML link', 'jcd' ) ),
					'<a href="#" class="jcd-fb-add-field" style="padding-left: 0;">' . esc_html__( 'Click here', 'jcd' ) . '</a>'
				); ?></p>
				<div class="clear"></div>
			</div>
			<!-- #jcd-fb-desc -->
			
			<div id="jcd-fb-email-desc" class="jcd-fb-desc" style="display: none;">
				<h3><?php esc_html_e( 'Do I need to fill this out?', 'jcd' ); ?></h3>
				<p><?php esc_html_e( 'Nope.  However, if you&#8217;d like to modify where your feedback is sent, or the subject line you can.  If you don&#8217;t make any changes here, feedback will be sent to the author of the page/post and the subject will be the name of this page/post.', 'jcd' ); ?></p>
				<div class="clear"></div>
			</div>
			<!-- #jcd-fb-email-desc -->

			<div id="jcd-fb-add-field" style="display: none">
				<h3><?php esc_html_e( 'Edit this new field', 'jcd' ); ?></h3>
			
				<label for="jcd-fb-new-label"><?php esc_html_e( 'Label', 'jcd' ); ?></label>
				<input type="text" id="jcd-fb-new-label" value="<?php esc_attr_e( 'New field', 'jcd' ); ?>" />
				
				<label for="jcd-fb-new-type"><?php esc_html_e( 'Field type', 'jcd' ); ?></label>
				<div class="select_wrapper">
					<select id="jcd-fb-new-type" class="jcd-input">
						<option value="checkbox"><?php esc_html_e( 'Checkbox', 'jcd' ); ?></option>
						<option value="select"><?php esc_html_e( 'Drop down', 'jcd' ); ?></option>
						<option value="email"><?php esc_html_e( 'Email', 'jcd' ); ?></option>
						<option value="name"><?php esc_html_e( 'Name', 'jcd' ); ?></option>
						<option value="radio"><?php esc_html_e( 'Radio', 'jcd' ); ?></option>
						<option value="text" selected="selected"><?php esc_html_e( 'Text', 'jcd' ); ?></option>
						<option value="textarea"><?php esc_html_e( 'Textarea', 'jcd' ); ?></option>
						<option value="url"><?php esc_html_e( 'Website', 'jcd' ); ?></option>
					</select>
				</div>
				<div class="clear"></div>
				
				<div id="jcd-fb-options" style="display: none;">
					<div id="jcd-fb-new-options">
						<label for="jcd-fb-option0"><?php esc_html_e( 'Options', 'jcd' ); ?></label>
						<input type="text" id="jcd-fb-option0" optionid="0" value="<?php esc_attr_e( 'First option', 'jcd' ); ?>" class="jcd-fb-options" />
					</div>
					<div id="jcd-fb-add-option" class="jcd-fb-add-option">
						<a href="#" id="jcd-fb-another-option"><?php esc_html_e( 'Add another option', 'jcd' ); ?></a>
					</div>
				</div>
				
				<div class="jcd-fb-required">
					<label></label>
					<input type="checkbox" id="jcd-fb-new-required" />
					<label for="jcd-fb-new-required" class="jcd-fb-radio-label"><?php esc_html_e( 'Required?', 'jcd' ); ?></label>
					<div class="clear"></div>
				</div>
				
				<input type="hidden" id="jcd-fb-field-id" />
				<input type="submit" class="button button-primary button-small" value="<?php esc_attr_e( 'Save this field', 'jcd' ); ?>" id="jcd-fb-save-field" name="save">
			</div>
			<!-- #jcd-fb-add-field -->
		</div>
		<!-- .jcd-fb-right -->
		
		<form id="jcd-fb-preview">
			<div id="jcd-fb-preview-form" class="jcd-fb-container">
				<h1><?php esc_html_e( 'Here&#8217;s what your form will look like', 'jcd' ); ?></h1>
				<div id="sortable" class="jcd-fb-form-case">
					
					<div id="jcd-fb-extra-fields" class="jcd-fb-extra-fields"></div>
					
					<a href="#" id="jcd-fb-new-field" class="jcd-fb-add-field"><?php esc_html_e( 'Add a new field', 'jcd' ); ?></a>
				</div>
				<input type="submit" class="button-primary" tabindex="4" value="<?php esc_attr_e( 'Add this form to my post', 'jcd' ); ?>" id="jcd-fb-save-form" name="save">
			</div>
			<!-- #jcd-fb-preview-form -->

			<div id="jcd-fb-email-settings" class="jcd-fb-container" style="display: none;">
				<h1><?php esc_html_e( 'Email settings', 'jcd' ); ?></h1>
				<div class="jcd-fb-form-case jcd-fb-settings">
					<label for="jcd-fb-fieldname"><?php esc_html_e( 'Enter your email address', 'jcd' ); ?></label>
					<input type="text" id="jcd-fb-field-my-email" style="background: #FFF !important;" />

					<label for="jcd-fb-fieldemail" style="margin-top: 14px;"><?php esc_html_e( 'What should the subject line be?', 'jcd' ); ?></label>
					<input type="text" id="jcd-fb-field-subject" style="background: #FFF !important;" />
				</div>
				<input type="submit" class="button-primary" value="<?php esc_attr_e( 'Save and go back to form builder', 'jcd' ); ?>" id="jcd-fb-prev-form" name="save">
			</div>
			<!-- #jcd-fb-email-settings -->
		</form>
		<!-- #jcd-fb-preview -->

	</body>

</html>
<?php
/**
 * Template Name: Maintenance Page
 * 
 */
global $jcd_options;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title><?php jcd_title(); ?></title>
		
		<?php wp_head(); ?>
		<?php jcd_head(); ?>
	</head>
	
	<body class="maintenance-page">

		<div class="container">
			<div class="maintenance-wrapper">
				
				<div class="maintenance-header">
					<h1><?php _e('Maintenance Mode', 'jcd') ?></h1>
				</div>

				<div class="maintenance-text">
					<p><?php _e('Briefly unavailable for scheduled maintenance. Check back in a minute.', 'jcd'); ?></p>
					<?php if( get_option( 'jcd_maintenance_text' ) ) : ?>
						<p><?php echo get_option( 'jcd_maintenance_text' ); ?></p>
					<?php endif; ?>
				</div>
				
				<?php
					$maintenance_extra = apply_filters( 'jcd_maintenance_extra_content', '' );
					if( $maintenance_extra ) {
						echo "<div class='maintenance-extra-content'>{$maintenance_extra}</div>";
					}
				?>

			</div>
		</div>

	</body>
	
	<script type="text/javascript">
		(function($){

			<?php 
			$background_image = get_option( 'jcd_maintenance_background' );
			if( $background_image != '' ) : ?>
				$.anystretch("<?php echo $background_image; ?>");
			<?php endif; ?>

		})(jQuery);
	</script>

	<?php //wp_footer(); ?>
</html>
